# Arkiee.website

Arkiee.website is Arkiee's website which is based on these opened source libraries and tools:

  - Vue.js 2.6.10, Jquery 3.4.1, jquery.cookie.js, Summernote 0.8.12 and Bootstrap 4.3.1 with Popper.js for Front-end development
  - Java 1.8, Spring (Boot 2.1.6), Spring DATA JPA/Hiberate, aws-java-sdk-s3 1.11.580, commons-fileupload 1.4, Lombok and Gradle 5.4.1 for Back-end development
  - MariaDB 10.2 as Database provided and powered by AWS
  - Redis 5.0 as Cache Server provided and powered by AWS
  - AWS S3, RDS, EC2, ElastiCache for Server
  - Jenkins, Git and Gitlab for Build Automation
(Continuous Integration and Continuous Delivery)
  - Centos 7 for Jenkins and Ubuntu 18.04 on AWS for Server
  - IntelliJ IDEA on Windows 10 RS4 for generating code
  - ~~Visual Code on Windows 10 RS4 for generating code~~

© 2019 Arkiee(Seungwoo Yu) - All Rights Reserved