// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
package com.arkiee.website;

import com.arkiee.website.utils.Sha512;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class SHATests {

    Logger logger = LoggerFactory.getLogger(SHATests.class);
    @Test
    public void IndexModelTests() {
        String input = "Arkiee_SHA_Test";
        String salt = Sha512.generateSalt();
        String encryptedInput = Sha512.getEncrypt(input, salt);
        logger.debug(encryptedInput);
    }
    
}