// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
package com.arkiee.website;

// Imports fundamental classes provided by Spring

import com.arkiee.website.models.dto.UserDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// Imports DTO for testing

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebsiteApplication.class)
public class UserDTOTests {

    Logger logger = LoggerFactory.getLogger(UserDTOTests.class);

	@Test
	public void convertToObjectTests() {
        UserDTO testUserDTO = new UserDTO();
        testUserDTO.setUserId("ID");
        testUserDTO.setUserPw("PW");
        testUserDTO.setUserName("Name");
        //testUserDTO.setUserEmial("a@a.com"); - uninjected because of test

        logger.debug(testUserDTO.toHashMap().toString());
    }
}