// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
package com.arkiee.website;

import com.arkiee.website.models.dto.UserDTO;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.utils.Sha512;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebsiteApplication.class)
public class UserDAOTests {

    private Logger logger = LoggerFactory.getLogger(UserDAOTests.class);

    @Autowired
    private UserDAO testUserDAO;

    private UserDTO testUserDTO = new UserDTO();

    @Test
    public void insertTests() throws Exception {
        String originalPw = "Passpass1234-";
        String salt = Sha512.generateSalt();
        String encryptedInput = Sha512.getEncrypt(originalPw, salt);

        testUserDTO.setUserId("HAHAHa")
            .setUserPw(encryptedInput)
            .setUserPwSalt(salt)
            .setUserName("NAMELESS")
            .setUserEmail("NAME@LESS.COM");
        
        try {
            logger.debug(testUserDTO.toString());
            testUserDAO.insert(testUserDTO);
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void selectByInputTests() {
        testUserDTO.setUserId("HAHAHa");
        try {
            List<UserDTO> list = testUserDAO.selectByInput(testUserDTO);
            for (UserDTO userDTO : list) {
                System.out.println(userDTO.getUserId());
            }
        } catch (DataAccessException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void selectAllRowsTests() throws Exception {
        try {
            List<UserDTO> list = testUserDAO.selectAllRows();
            for (UserDTO userDTO : list) {
                System.out.println(String.join(", ",
                        userDTO.getUserId(),
                        userDTO.getUserName(),
                        userDTO.getUserEmail())
                );
            }
        } catch (DataAccessException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void updateByInputTests() {
        try {
            String originalPw = "newPassword1234-";
            String salt = Sha512.generateSalt();
            String encryptedInput = Sha512.getEncrypt(originalPw, salt);

            testUserDTO.setUserId("HAHAHa")
            .setUserPw(encryptedInput)
            .setUserPwSalt(salt)
            .setUserName("LESSNAME")
            .setUserEmail("LESS@NAME.COM");

            testUserDAO.updateByInput(testUserDTO);
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteTests() {
        try {
            testUserDTO.setUserId("HAHAHa");
            testUserDAO.delete(testUserDTO);
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
    }
}