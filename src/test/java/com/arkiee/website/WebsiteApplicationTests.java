// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
package com.arkiee.website;

// Imports fundamental Spring classes
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebsiteApplication.class)
public class WebsiteApplicationTests {

	@Test
	public void contextLoads() {
	}

}
