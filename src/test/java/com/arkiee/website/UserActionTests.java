// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
package com.arkiee.website;

import com.arkiee.website.models.LoginModel;
import com.arkiee.website.models.dto.UserDTO;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.repositories.jpa.UserGroupRepository;
import com.arkiee.website.services.LoginService;
import com.arkiee.website.services.RegisterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebsiteApplication.class)
public class UserActionTests {

    Logger logger = LoggerFactory.getLogger(UserDAOTests.class);

    @Autowired
    UserDAO testUserDAO;
    @Autowired
    MockHttpSession session;
    @Autowired
    MockHttpServletResponse response;
    @Autowired
    MockHttpServletRequest request;
    @Autowired
    UserGroupRepository userGroupRepository;

    private ObjectMapper objectMapper = new ObjectMapper();
    UserDTO testUserDTO = new UserDTO();
    LoginModel testModel;

    @Test
    public void registerTests() throws Exception {
        testUserDTO.setUserId("HAHAHa")
            .setUserPw("Passpass1234-")
            .setUserName("NAMELESS")
            .setUserEmail("NAME@LESS.COM");
        RegisterService registerService = new RegisterService(request, testUserDAO, testUserDTO, userGroupRepository);
        try {
            System.out.println(registerService.register());
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void loginTests() throws Exception {
        testModel = new LoginModel("HAHAHa", "Passpass1234-", false);
        String mockedRequest = objectMapper.writeValueAsString(testModel);
        request.addHeader("loginProcess", mockedRequest);
        LoginService loginService = new LoginService(request, response, session, testUserDAO);
        try {
            System.out.println(loginService.login());
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void logoutTests() throws Exception {
        testModel = new LoginModel("HAHAHa", "Passpass1234-", false);
        String mockedRequest = objectMapper.writeValueAsString(testModel);
        request.addHeader("loginProcess", mockedRequest);
        LoginService loginService = new LoginService(request, response, session, testUserDAO);
        loginService.login();
        System.out.println(loginService.logout());
    }

    @Test
    public void unregisterTests() throws Exception {
        testModel = new LoginModel("HAHAHa", "Passpass1234-", false);
        String mockedRequest = objectMapper.writeValueAsString(testModel);
        request.addHeader("loginProcess", mockedRequest);
        LoginService loginService = new LoginService(request, response, session, testUserDAO);
        loginService.login();

        session.setAttribute("Id", "HAHAHa");
        session.setAttribute("Credential", "TESTONLY");
        RegisterService registerService = new RegisterService(request, session, testUserDAO, userGroupRepository);
        System.out.println(registerService.unregister() + " " + loginService.logout());
    }
}