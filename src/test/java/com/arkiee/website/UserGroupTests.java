// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
package com.arkiee.website;

import com.arkiee.website.models.entities.UserGroupEntity;
import com.arkiee.website.repositories.jpa.UserGroupRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebsiteApplication.class)
public class UserGroupTests {

    Logger logger = LoggerFactory.getLogger(UserDTOTests.class);

    @Autowired
    UserGroupRepository repository;

    @Test
    public void addValueIntoUserGroup() {
        UserGroupEntity testUserGroup = UserGroupEntity.builder().userId("HAHAHa")
                                                    .userGroup("USER")
                                                    .build();
        repository.save(testUserGroup);
    }
}