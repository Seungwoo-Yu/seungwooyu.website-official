﻿// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
const koreanManager = new function() {
    this.common = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.about = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.error = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.lecture = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.login = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.profile = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.project = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.register = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
};
// Language data for common use
koreanManager.common.error = {
    notSatisfied: "{name}을(를) 바르게 입력해주세요.\n",
    notMatched: "{name}이(가) 일치하지 않습니다.",
    required: "{name}은(는) 필수입력 항목입니다.\n",
    existed: "{name}이(가) 이미 존재합니다.",
    exceedSizeLimitation: "파일(들)이 500M보다 작아야합니다",
};
koreanManager.common.info = {
    passed: "좋아보이네요!",
    matched: "{name}이(가) 일치합니다.",
    commentHere: "여기에 댓글을 남겨보세요!",
    totalComments: "총 {name} 개의 댓글이 달렸습니다"
};
koreanManager.common.require = {
    id: "숫자, 대소문자 영어를 포함한 6글자에서 15글자 사이어야 합니다.",
    password: "숫자, 대소문자 영어, 특수문자를 포함한 10글자에서 30글자 사이어야 합니다.",
    username: "숫자, 대문자 혹은 소문자 영어나 한글을 포함한 4글자에서 20글자 사이어야 합니다.",
    commentLogin: "댓글을 달기 전, 로그인이 필요합니다",
};
koreanManager.common.button = {
    tabLogin: "로그인",
    tabRegistration: "회원가입",
    tabProfile: "프로필",
    tabManager: "관리도구",
    menuSignin: "로그인",
    menuSignup: "회원가입",
    menuSignout: "로그아웃",
    menuProfile: "프로필",
    confirmComment: "확인",
    cancelComment: "취소",
    deleteComment: "삭제",
    editComment : "수정",
    replyComment: "답글",
    addPost: "글쓰기",
    deletePost: "삭제",
    confirmPost: "완료",
    cancelPost: "취소",
    editPost: "수정",
    modalYes: "네",
    modalNo: "잠시만요!",
};
koreanManager.common.text = {
    id: "아이디",
    password: "비밀번호",
    passwordConfirm: "비밀번호 확인",
    username: "닉네임",
    email: "이메일",
    title: "제목",
    author: "글쓴이 : ",
    search: "검색"
};
// Language data for Error
koreanManager.error.button = {
    goBack: "돌아가기"
};
koreanManager.error.text = {
    errorTitle: "에러",
    errorSubtitle: "이런, 오류가 발생했네요!",
    errorDescription1: "Arkiee에게 문의해주세요.",
    errorDescription2: "아래 로그가 도와줄 거예요!",
};
// Language data for Lecture
koreanManager.lecture.button = {
    showComment: "더보기",
    hideComment: "숨기기",
    continue: "여기를 눌러 다음글 보기",
};
koreanManager.lecture.info = {
    modalTitle: "알림",
    modalRemoveOne: "이 글을 지우시겠습니까?\n이 작업은 다시 되돌릴 수 없습니다.",
};
// Language data for Login
koreanManager.login.error = {
    error: "로그인에 실패했습니다.\n 다시 시도해주세요.",
};
koreanManager.login.info = {
    registered: "계정이 생성되었습니다.\n 로그인 해 볼까요?"
};
koreanManager.login.button = {
    continue: "로그인",
};
koreanManager.login.text = {
    loginTitle: "어서오세요!",
    autologin: "로그인 한 채로 유지",
};
// Language data for Profile
koreanManager.profile.error = {
    error: "정보를 변경할 수 없습니다\n다시 시도하십시오",
}
koreanManager.profile.info = {
    modalTitle: "알림",
    modalUnregister: "영구히 계정을 삭제하시겠습니까?\n이 작업은 다시 되돌릴 수 없습니다.",
    changed: "정보가 변경되었습니다",
};
koreanManager.profile.button = {
    unregister: "계정 탈퇴",
    changeInformation: "계정정보 변경",
    confirmChange: "변경내용 저장",
    cancelChange: "취소",
    changePassword: "비밀번호 변경",
};
koreanManager.profile.text = {
    profileTitle: "프로필 설정",
};
// Language data for Project
koreanManager.project.info = {
    warning: "업로드 중...\n업로드 중에 다른 파일을 업로드하거나 창을 닫지 마세요",
    modalTitle: "알림",
    modalRemoveOne: "이 글을 지우시겠습니까?\n이 작업은 다시 되돌릴 수 없습니다.",
};
koreanManager.project.text = {
    subtitle: "새 프로젝트 글쓰기",
    subtitleDescription: "부제",
    editSubtitle: "프로젝트 글 수정",
    videoUpload: "비디오 업로드",
    target: "여기에 비디오 삽입 : ",
    fileUpload: "파일 업로드",
};
// Language data for Register
koreanManager.register.button = {
    continue: "회원가입",
};
koreanManager.register.text = {
    registerTitle: "새 계정 만들기",
    id: "아이디",
    password: "비밀번호",
    passwordConfirm: "비밀번호 확인",
    username: "닉네임",
    email: "이메일",
};