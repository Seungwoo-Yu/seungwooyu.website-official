﻿// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
const englishManager = new function() {
    this.common = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.about = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.error = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.lecture = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.login = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.profile = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.project = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
    this.register = {
        error: {},
        info: {},
        require: {},
        button: {},
        text: {}
    };
};
// Language data for common use
englishManager.common.error = {
    notSatisfied: "Please enter a vaild {name}.\n",
    notMatched: "{name} is not matched.",
    required: "{name} is required.\n",
    existed: "{name} is already existed.",
    exceedSizeLimitation: "The file(s) must be less than 500M",
};
englishManager.common.info = {
    passed: "Seems good!",
    matched: "{name} is matched.",
    commentHere: "Leave a comment here!",
    totalComments: "Total comments: {name}"
};
englishManager.common.require = {
    id: "must be 6 to 15 digits and mixed of uppercase, lowercase and number.",
    password: "must be 10 to 30 digits and mixed of uppercase, lowercase, number and non-alphanumeric symbols.",
    username: "must be 4 to 20 digits and mixed of uppercase or lowercase and number or korean character.",
    commentLogin: "You need to login before leaving a comment",
};
englishManager.common.button = {
    tabLogin: "Login",
    tabRegistration: "Registration",
    tabProfile: "Profile",
    tabManager: "Manager",
    menuSignin: "Sign in",
    menuSignup: "Sign up",
    menuSignout: "Sign out",
    menuProfile: "Profile",
    confirmComment: "Write",
    cancelComment: "Cancel",
    deleteComment: "Delete",
    editComment : "Edit",
    replyComment: "Reply",
    addPost: "Add",
    deletePost: "Delete",
    confirmPost: "Confirm",
    cancelPost: "Cancel",
    editPost: "Edit",
    modalYes: "Okay I'm sure",
    modalNo: "No! Wait a minute",
};
englishManager.common.text = {
    id: "User Id",
    password: "User password",
    passwordConfirm: "Confirm password",
    username: "User name",
    email: "User email",
    title: "Title",
    author: "Written by : ",
    search: "Search",
};
// Language data for Error
englishManager.error.button = {
    goBack: "Go back"
};
englishManager.error.text = {
    errorTitle: "ERROR",
    errorSubtitle: "Ooof! We got in trouble!",
    errorDescription1: "You should make sure Arkiee gets realised about this issue.",
    errorDescription2: "The below error log might be help.",
};
// Language data for Lecture
englishManager.lecture.button = {
    showComment: "See more",
    hideComment: "Hide",
    continue: "Click to continue",
};
englishManager.lecture.info = {
    modalTitle: "Warning",
    modalRemoveOne: "It will remove this lecture post permanently.\n Are you sure with that?",
};
// Language data for Login
englishManager.login.error = {
    error: "Failed to login. Try again",
};
englishManager.login.info = {
    registered: "Created an account. Let's go in!"
};
englishManager.login.button = {
    continue: "Continue",
};
englishManager.login.text = {
    loginTitle: "Welcome Back!",
    autologin: "Stay logged in",
};
// Language data for Profile
englishManager.profile.error = {
    error: "Your information couldn't be changed\nPlease try again",
}
englishManager.profile.info = {
    modalTitle: "Warning",
    modalUnregister: "It will remove your account permanently.\n Are you sure with that?",
    changed: "Your information has been changed",
};
englishManager.profile.button = {
    unregister: "Unregister account",
    changeInformation: "Change information",
    confirmChange: "Confirm changes",
    cancelChange: "Cancel changes",
    changePassword: "Change password",
};
englishManager.profile.text = {
    profileTitle: "Profile setting",
};
// Language data for Project
englishManager.project.info = {
    warning: "Uploading in progress\nDO NOT CLOSE THIS TAB AND UPLOAD SOMETHING ELSE when uploading",
    modalTitle: "Warning",
    modalRemoveOne: "It will remove this project post permanently.\n Are you sure with that?",
};
englishManager.project.text = {
    subtitle: "Write a new project",
    subtitleDescription: "Subtitle",
    editSubtitle: "Edit the project",
    videoUpload: "Video upload",
    target: "Put video at : ",
    fileUpload: "File upload",
};
// Language data for Register
englishManager.register.button = {
    continue: "Continue",
};
englishManager.register.text = {
    registerTitle: "Create a new account",
    id: "Id",
    password: "Password",
    passwordConfirm: "Confirm password",
    username: "Username",
    email: "Email",
};