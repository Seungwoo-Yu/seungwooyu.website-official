// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    let ajax = new AjaxManager();
    ajax.success = () => {
        $(location).attr("href", "/");
    };
    ajax.isValid("/session", "HEAD", "Credential");

    const registerForm = new Vue({
        el: ".body",
        data: {
            registerTitle: languageManager.loadedLanguage.register.text.registerTitle,
            formList: [
                {name: languageManager.loadedLanguage.register.text.id, tag: "user_id",
                    attributes: ["needDBCheck", "needRegExr", "noRepeat", "required", "noPassword"],
                    input: "", rule: idRule, error: undefined, warningSign: ".warning_user_id",
                    allowedSign: ".allowed_user_id", requireType: "id"},
                {name: languageManager.loadedLanguage.register.text.password, tag: "user_password",
                    attributes: ["noDBCheck", "needRegExr", "noRepeat", "required", "password"],
                    input: "", rule: passwordRule, error: undefined, warningSign: ".warning_user_password",
                    allowedSign: ".allowed_user_password", requireType: "password"},
                {name: languageManager.loadedLanguage.register.text.passwordConfirm, tag: "user_password_confirm",
                    attributes: ["noDBCheck", "noRegExr", "repeat", "required", "password"],
                    input: "", rule: undefined, error: undefined, warningSign: ".warning_user_password_confirm",
                    allowedSign: ".allowed_user_password_confirm", requireType: undefined},
                {name: languageManager.loadedLanguage.register.text.username, tag: "user_name",
                    attributes: ["needDBCheck", "needRegExr", "noRepeat", "required", "noPassword"],
                    input: "", rule: usernameRule, error: undefined, warningSign: ".warning_user_name",
                    allowedSign: ".allowed_user_name", requireType: "username"},
                {name: languageManager.loadedLanguage.register.text.email, tag: "user_email",
                    attributes: ["needDBCheck", "needRegExr", "noRepeat", "required", "noPassword"],
                    input: "", rule: emailRule, error: undefined, warningSign: ".warning_user_email",
                    allowedSign: ".allowed_user_email", requireType: undefined}
            ],
            registerButton: [
                {name: languageManager.loadedLanguage.register.button.continue, url: '/register'}
            ]
        },
        mounted: function() {
            let vm = this;
            $(document).on('keypress', function(e) {
                if(e.which === 13) {
                    vm.check(vm.formList);
                }
            });
            this.formList.forEach((list) => {
                if(list.attributes[4] === "password") {
                    $(".form_" + list.tag).attr("type", "password");
                }
            });
        },
        methods: {
            inputEvent(list, input) {
                list.error = undefined;
                if(list.attributes[1] === "needRegExr" && list.rule !== undefined) {
                    if(!list.rule.test(input)) {
                        list.error = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.error.notSatisfied);
                    }
                }
                if(list.attributes[2] === "repeat" && registerForm.formList[1].input !== input) {
                    list.error = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.error.notMatched);
                } else if(list.attributes[2] === "repeat" && registerForm.formList[1].error !== undefined) {
                    list.error = messageManager.getMessage(registerForm.formList[1].name, languageManager.loadedLanguage.common.error.notSatisfied);
                }
                if(list.attributes[3] === "required" && input === "") {
                    list.error = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.error.required);
                }
                if(list.error === undefined && list.tag === "user_password" && registerForm.formList[2].input === list.input) {
                    const message = messageManager.getMessage(registerForm.formList[2].name, languageManager.loadedLanguage.common.info.matched);
                    textManager.setText(registerForm.formList[2].allowedSign, message, false);
                    registerForm.formList[2].error = undefined;
                } else if (list.error === undefined && list.tag === "user_password" && registerForm.formList[2].input !== list.input) {
                    registerForm.formList[2].error = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.error.notMatched);
                }
                if(list.error !== undefined) {
                    textManager.setText($(list.warningSign), list.error, false);
                    if(list.attributes[1] === "needRegExr" && list.requireType !== undefined) {
                        textManager.setText($(list.warningSign), languageManager.loadedLanguage.common.require[list.requireType], true);
                    }
                } else if (list.tag === "user_password") {
                    textManager.setText(list.allowedSign, languageManager.loadedLanguage.common.info.passed, false);
                } else if (list.tag === "user_password_confirm") {
                    const message = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.info.matched);
                    textManager.setText(list.allowedSign, message, false);
                }
            },
            changeEvent(list, input) {
                if(list.attributes[0] === "needDBCheck" && input !== "" && list.error === undefined) {
                    let ajax = new AjaxManager();
                    ajax.callback = (resultData) => {
                        if(!resultData.condition || resultData.condition === undefined) {
                            list.error = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.error.existed);
                            textManager.setText($(list.warningSign), list.error, false);
                        } else {
                            textManager.setText(list.allowedSign, languageManager.loadedLanguage.common.info.passed, false);
                        }
                    };
                    ajax.getAjaxResponse("/register/check", "GET", null, "json", "application/json", (xhr) => {
                        xhr.setRequestHeader('registerCheck', JSON.stringify({"type" : list.tag, "content" : input}));
                    });
                }
            },
            check(formList) {
                let isAvailable = true;
                formList.forEach((list) => {
                    registerForm.inputEvent(list, list.input);
                    if(list.error !== undefined) {
                        isAvailable = false;
                    }
                });
                if(isAvailable) {
                    const queue = JSON.stringify({
                        "userId": formList[0].input,
                        "userPw": formList[1].input,
                        "userPwSalt": "",
                        "userName": formList[3].input,
                        "userEmail": formList[4].input
                    });
                    let ajax = new AjaxManager();
                    ajax.callback = (resultData) => {
                        $(location).attr("href", resultData);
                    };
                    ajax.getAjaxResponse("/register/process", "POST", queue, "text", "application/json");
                }
            },
        }
    });
});