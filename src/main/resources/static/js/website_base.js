// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
"use strict";
const textManager = new function() {
    this.setText = (undefined, text, isAppend = false) => {
        if(typeof undefined === 'string') {
            return this.setTextByString(undefined, text, isAppend);
        } else if (typeof undefined === 'object') {
            return this.setTextByObject(undefined, text, isAppend);
        }
    };
    this.setTextByString = (location, text, isAppend) => {
        if(isAppend){
            $(location).append(text);
        } else {
            $(location).text(text);
        }
    };
    this.setTextByObject = (object, text, isAppend) => {
        if(isAppend){
            object.append(text);
        } else {
            object.text(text);
        }
    };
};

class AjaxManager {
    constructor() {
        this.callback;
        this.error;
        this.success;
        this.failure;
    }
    getAjaxResponse(url, type, data, dataType, contentType, beforeSend, processData, xhr) {
        let ajax = $.ajax({
            url: url,
            type: type,
            data: data,
            dataType: dataType,
            contentType: contentType,
            beforeSend: beforeSend,
            processData: processData,
            xhr: xhr,
            success: this.callback,
            error: this.error
        });
    };
    workQueue(url, type, searchQueue) {
        this.callback = (data, textStatus, response) => {
            if(response.getResponseHeader(searchQueue) != null) {
                if(this.success) {
                    this.success(data, textStatus, response);
                }
            } else {
                if(this.failure) {
                    this.failure(data, textStatus, response);
                }
            }
        };
        this.getAjaxResponse(url, type);
    };
    isValid(url, type, searchQueue) {
        this.workQueue(url, type, searchQueue);
    };
}

class DateManager {
    constructor() {
        this.data;
        this.type;
    };

    getDate() {
        let replicatedData = this.data;
        replicatedData.forEach((element) => {
            let localTimeData = new Date(0);
            if(this.type === "comment") {
                localTimeData.setSeconds(element.commentDate.epochSecond);
                //localTimeData.setSeconds(element.commentDate.epochSecond + 540 * 60);
            } else if(this.type === "lecture") {
                localTimeData.setSeconds(element.lectureDate.epochSecond);
                //localTimeData.setSeconds(element.lectureDate.epochSecond + 540 * 60);
            }
            let dateBlocks = [localTimeData.getDay(), localTimeData.getMonth() + 1, localTimeData.getFullYear()];
            let timeBlocks = [localTimeData.getHours(), localTimeData.getMinutes()];
            if($.cookie("languagePreference") === "kr") {
                dateBlocks = [localTimeData.getFullYear(), localTimeData.getMonth() + 1, localTimeData.getDay()];
            }
            for(let i = 0; i < dateBlocks.length; i++) {
                if(dateBlocks[i] < 10) {
                    dateBlocks[i] = '0' + dateBlocks[i];
                }
            }
            for(let i = 0; i < timeBlocks.length; i++) {
                if(timeBlocks[i] < 10) {
                    timeBlocks[i] = '0' + timeBlocks[i];
                }
            }
            let tempData;
            tempData = dateBlocks.join("/");
            if($.cookie("languagePreference") === "kr") {
                tempData = dateBlocks.join("-");
            }
            tempData += " " + timeBlocks.join(":");
            if(this.type === "comment") {
                element.commentDate = tempData;
            } else if(this.type === "lecture") {
                element.lectureDate = tempData;
            }
        });
        return replicatedData;
    };
}

class CommentManager {
    commentLeaveEvent(text, linkedTable, linkedTableId) {
        if(text !== "") {
            const queue = JSON.stringify({
                entity: {
                    commentLinkedTable: linkedTable,
                    commentLinkedTableId: linkedTableId,
                    commentLinkedComment: 0,
                    commentContents: text,
                }
            });
            let ajax = new AjaxManager();
            ajax.callback = () => {
                $(location).attr("href", "");
            };
            ajax.getAjaxResponse("/comments/" + linkedTableId, "POST", queue, "text", "application/json", null);
        }
    };
    commentPropertyReflector(index, boolean) {
        $(".commentEdit_comment_" + index).removeAttr("id");
        $(".commentDelete_comment_" + index).removeAttr("id");
        $(".commentConfirm_edit_comment_" + index).attr("id", "disabled");
        $(".commentCancel_edit_comment_" + index).attr("id", "disabled");
        if(boolean) {
            $(".commentEdit_comment_" + index).attr("id", "disabled");
            $(".commentDelete_comment_" + index).attr("id", "disabled");
            $(".commentConfirm_edit_comment_" + index).removeAttr("id");
            $(".commentCancel_edit_comment_" + index).removeAttr("id");
        }
    };
    commentEditEvent(editMode, index) {
        Vue.set(editMode, index, {enabled: true});
        this.commentPropertyReflector(index, true);
    }
    commentDeleteEvent(data, index, comment, url) {
        const queue = JSON.stringify({
            entity: {
                commentId: comment,
            }
        });
        let ajax = new AjaxManager();
        ajax.callback = () => {
            data.splice(index, 1);
        };
        ajax.getAjaxResponse("/comments/" + url, "DELETE", queue, "text", "application/json", null);
    };
    commentConfirmEditEvent(editMode, data, index, comment, url) {
        Vue.set(editMode, index, {enabled: false});
        let contentsData = data;
        this.commentPropertyReflector(index, false);
        const queue = JSON.stringify({
            entity: {
                commentId: comment,
                commentContents: contentsData
            }
        });
        let ajax = new AjaxManager();
        ajax.getAjaxResponse("/comments/" + url, "PATCH", queue, "text", "application/json", null);
    };
    commentCancelEditEvent(editMode, index) {
        Vue.set(editMode, index, {enabled: false});
        this.commentPropertyReflector(index, false);
    };
}

const messageManager = new function() {
    this.errorFormat;
    this.infoFormat;
    this.requireType;
    this.getMessage = (target, format) => {
        const messages = format.split("{name}");
        const mixed = messages[0].concat(target, messages[1]);
        return mixed;
    };
};

const languageManager = new function() {
    this.loadedLanguage;
    this.languageLoaded = [];
    this.selectLanguage = (toggleButton, selection, cookieKey, cookieValue) => {
        textManager.setText(toggleButton, selection.text(), false);
        selection.hide();
        $.cookie(cookieKey, cookieValue, {path: '/'});
    };
    this.restoreChosenLanguage = (selection) => {
        selection.show();
    };
    this.setLanguage = (value) => {
        if(value === "kr") {
            this.loadedLanguage = koreanManager;
        } else {
            this.loadedLanguage = englishManager;
        }
    }
};
if(($.cookie("languagePreference") === null && navigator.language.split("-")[0].toLowerCase() === "kr") || $.cookie("languagePreference") === "kr") {
    $.cookie('languagePreference', "kr", {path: '/'});
    languageManager.setLanguage("kr");
} else {
    $.cookie('languagePreference', "en", {path: '/'});
    languageManager.setLanguage("en");
}

const currentURL = window.location.href.split("/");
const idRule = /^.*(?=.*[a-z])(?=.*[A-Z])(?!.*[ ]).{6,15}$/;
const passwordRule = /^.*(?=^.{10,30})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+_=-])(?!.*[ ]).*$/;
const usernameRule = /^.*(?=.*[a-z])(?=.*[A-Z])(?!.*[ ]).{4,20}$|^.*(?=.*[0-9])(?=.*[a-z])(?!.*[ ]).{4,20}$|^.*(?=.*[0-9])(?=.*[A-Z])(?!.*[ ]).{4,20}$|^.*(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?!.*[ ]).{4,20}$|^.*(?=.*[ㄱ-ㅎㅏ-ㅣ|가-힣])(?!.*[ ]).{4,20}$|^.*(?=.*[0-9])(?=.*[ㄱ-ㅎㅏ-ㅣ|가-힣])(?!.*[ ]).{4,20}$|^.*(?=.*[0-9])(?=.*[a-z])(?=.*[ㄱ-ㅎㅏ-ㅣ|가-힣])(?!.*[ ]).{4,20}$|.*(?=.*[0-9])(?=.*[A-Z])(?=.*[ㄱ-ㅎㅏ-ㅣ|가-힣])(?!.*[ ]).{4,20}$|.*(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[ㄱ-ㅎㅏ-ㅣ|가-힣])(?!.*[ ]).{4,20}$/;
const emailRule = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,4}.$/i;

/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    let token = $.cookie('csrftoken');
    let header = "XSRF-TOKEN";
    $(document).ajaxSend(function(e, xhr) {
        xhr.setRequestHeader(header, token);
    });
    Vue.component('modal-indicator', (resolve) => {
        let ajax = new AjaxManager();
        ajax.callback = (data) => {
            resolve({
                template: data
            })
        };
        ajax.getAjaxResponse("/html/modal.html", "GET");
    });

    const title = new Vue({
        el: 'title',
        data: {
            title: "Arkiee's Website",
            subtitleList: [
                {name: 'About', url: 'about'},
                {name: 'Projects', url: 'projects'},
                {name: 'Lectures', url: 'lectures'},
                {name: languageManager.loadedLanguage.common.button.tabLogin, url: 'login'},
                {name: languageManager.loadedLanguage.common.button.tabRegistration, url: 'register'},
                {name: languageManager.loadedLanguage.common.button.tabProfile, url: 'profile'},
                {name: languageManager.loadedLanguage.common.button.tabManager, url: 'manager'}
            ],
            subtitle: "",
        },
        created: function() {
            this.subtitleList.forEach((subTitleURL) => {
                if(currentURL[3] !== "" && currentURL[3].length !== 0 && subTitleURL.url === currentURL[3]) {
                    this.subtitle = subTitleURL.name;
                }
            });
        }
    });

    if($('.menu').length){
        const menu = new Vue({
            el: '.menu',
            data: {
                menuList: [
                    {name: 'ABOUT', url: '/about'},
                    {name: 'PROJECTS', url: '/projects'},
                    {name: 'LECTURES', url: '/lectures'}
                ],
                loginButton1: [
                    {name: languageManager.loadedLanguage.common.button.menuSignin, url: '/login'}
                ],
                loginButton2: [
                    {name: languageManager.loadedLanguage.common.button.menuSignup, url: '/register'}
                ],
                logoutButton: [
                    {name: languageManager.loadedLanguage.common.button.menuSignout, url: '/login'}
                ],
                profileButton: [
                    {name: languageManager.loadedLanguage.common.button.menuProfile, url: '/profile'}
                ],
                sessionAvailability: false,
                session: undefined,
            },
            created: function() {
                let ajax = new AjaxManager();
                ajax.success = () => {
                    this.sessionAvailability = true;
                };
                ajax.isValid("/session", "HEAD", "Credential");
            },
            methods: {
                menuEvent(object, searchQueue) {
                    if(searchQueue === menu.loginButton2[0].name) {
                        let url = object.find((obj) => {return obj.name === searchQueue}).url;
                        $(location).attr("href", url);
                    }
                    if(searchQueue === menu.logoutButton[0].name) {
                        let url = object.find((obj) => {return obj.name === searchQueue}).url;
                        let ajax = new AjaxManager();
                        ajax.callback = () => {
                            $(location).attr("href", "/");
                        };
                        ajax.getAjaxResponse(url, "DELETE");
                    }
                }
            }
        });
    }

    const footer = new Vue({
        el: '.footer',
        data: {
            copyright: "© 2019 Arkiee - All Rights Reserved",
            languagePreference: [
                {name: 'English (영어)', tag: "englishPreference"},
                {name: 'Korean (한국어)', tag: "koreanPreference"},
            ]
        },
        methods: {
            menuEvent(object) {
                if(object === footer.languagePreference[0]) {
                    languageManager.selectLanguage($("#dropdownPreferenceToggle"), $(".englishPreference"), "languagePreference", "en");
                    languageManager.restoreChosenLanguage($(".koreanPreference"));
                }
                if(object === footer.languagePreference[1]) {
                    languageManager.selectLanguage($("#dropdownPreferenceToggle"), $(".koreanPreference"), "languagePreference", "kr");
                    languageManager.restoreChosenLanguage($(".englishPreference"));
                }
                $(location).attr("href", "");
            }
        },
        mounted: function() {
            if ($.cookie("languagePreference") === "kr") {
                languageManager.selectLanguage($("#dropdownPreferenceToggle"), $(".koreanPreference"), "languagePreference", "kr");
            } else {
                languageManager.selectLanguage($("#dropdownPreferenceToggle"), $(".englishPreference"), "languagePreference", "en");
            }
        }
    });
});