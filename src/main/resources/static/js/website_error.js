// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    const errorIndicator = new Vue({
        el: ".body",
        data: {
            errorTitle: languageManager.loadedLanguage.error.text.errorTitle,
            errorSubtitle: languageManager.loadedLanguage.error.text.errorSubtitle,
            errorDescriptionList: [
                {text: languageManager.loadedLanguage.error.text.errorDescription1},
                {text: languageManager.loadedLanguage.error.text.errorDescription2}
            ],
            errorButton: [
                {name: languageManager.loadedLanguage.error.button.goBack, url: '/'}
            ],
            errorUrl: window.location.href,
            errorPastUrl: document.referrer,
            errorTime: new Date($.now())
        },
        methods: {
            checkEvent(object) {
                $(location).attr("href", object.url);
            }
        }
    });
});