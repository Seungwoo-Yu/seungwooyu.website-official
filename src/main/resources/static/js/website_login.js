// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    let ajax = new AjaxManager();
    ajax.success = () => {
        $(location).attr("href", "/");
    };
    ajax.isValid("/session", "HEAD", "Credential");

    const loginForm = new Vue({
        el: ".body",
        data: {
            loginTitle: languageManager.loadedLanguage.login.text.loginTitle,
            formList: [
                {name: languageManager.loadedLanguage.common.text.id, tag: "user_id",
                    attributes: ["needDBCheck", "needRegExr", "noRepeat", "required", "noPassword"],
                    input: "", rule: idRule, error: undefined, warningSign: ".warning_user_id",
                    allowedSign: ".allowed_user_id", requireType: "id"},
                {name: languageManager.loadedLanguage.common.text.password, tag: "user_password",
                    attributes: ["noDBCheck", "needRegExr", "noRepeat", "required", "password"],
                    input: "", rule: passwordRule, error: undefined, warningSign: ".warning_user_password",
                    allowedSign: ".allowed_user_password", requireType: "password"}
            ],
            autoLoginCheckbox: [
                {name: languageManager.loadedLanguage.login.text.autologin, tag: "enableAutologin", input: false}
            ],
            loginButton: [
                {name: languageManager.loadedLanguage.login.button.continue, url: '/register'}
            ],
            indicatorList: [
                {url: "failure"},
                {url: "register"}
            ]
        },
        mounted: function() {
            let vm = this;
            $(document).on('keypress', function(e) {
                if(e.which === 13) {
                    vm.check(vm.formList);
                }
            });
            this.formList.forEach((list) => {
                if(list.attributes[4] === "password") {
                    $(".form_" + list.tag).attr("type", "password");
                }
            });
            if(currentURL[4] !== "" && currentURL[4] !== undefined) {
                if(currentURL[4] === this.indicatorList[0].url){
                    textManager.setText($(".loginIndicatorViewer"), languageManager.loadedLanguage.login.error.error, false);
                    $(".loginIndicator").removeAttr("id");
                } else if(currentURL[4] === "action" && currentURL[5] !== "" && currentURL[5] !== undefined) {
                    if(currentURL[5] === this.indicatorList[1].url) {
                        textManager.setText($(".loginIndicatorViewer"), languageManager.loadedLanguage.login.info.registered, false);
                        $(".loginIndicator").removeAttr("id");
                    }
                }
            }
        },
        methods: {
            inputEvent(list, input) {
                list.error = undefined;

                if(list.attributes[1] === "needRegExr" && list.rule !== undefined) {
                    if(!list.rule.test(input)) {
                        list.error = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.error.notSatisfied);
                    }
                }
                if(list.attributes[3] === "required" && input === "") {
                    list.error = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.error.required);
                }
                if(list.error !== undefined) {
                    $(list.warningSign).removeAttr("id");
                    textManager.setText($(list.warningSign), list.error, false);
                }
            },
            check(formList) {
                let isAvailable = true;
                formList.forEach((list) => {
                    loginForm.inputEvent(list, list.input);
                    if(list.error !== undefined) {
                        isAvailable = false;
                    }
                });
                if(isAvailable) {
                    const queue = JSON.stringify({
                        "userId": formList[0].input,
                        "userPw": formList[1].input,
                        "enableCookie": loginForm.autoLoginCheckbox[0].input,
                    });
                    let ajax = new AjaxManager();
                    ajax.callback = (resultData) => {
                        $(location).attr("href", resultData);
                    };
                    ajax.getAjaxResponse("/login/process", "GET", null, "text", "application/json", (xhr) => {
                        xhr.setRequestHeader('loginProcess', queue);
                    });
                }
            },
        }
    });
});