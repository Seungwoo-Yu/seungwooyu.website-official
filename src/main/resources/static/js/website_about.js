// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    const body = new Vue({
        el: ".body",
        data: {
            aboutTitle: languageManager.loadedLanguage.about.text.aboutTitle,
            aboutSubtitle: languageManager.loadedLanguage.about.text.aboutName,
            aboutContents: [
                {text: languageManager.loadedLanguage.about.text.aboutDescription1},
                {text: languageManager.loadedLanguage.about.text.aboutDescription2},
                {text: languageManager.loadedLanguage.about.text.aboutDescription3},
                {text: languageManager.loadedLanguage.about.text.aboutDescription4},
                {text: languageManager.loadedLanguage.about.text.aboutDescription5},
                {text: languageManager.loadedLanguage.about.text.aboutDescription6},
                {text: languageManager.loadedLanguage.about.text.aboutDescription7},
                {text: languageManager.loadedLanguage.about.text.aboutDescription8}
            ]
        }
    });
});