// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    let ajax = new AjaxManager();
    ajax.failure = () => {
        $(location).attr("href", "/");
    };
    ajax.isValid("/session", "HEAD", "Credential");

    const modal = new Vue({
        el: '.modalIndicator',
        data: {
            modalShow: false,
            modalHeader: languageManager.loadedLanguage.profile.info.modalTitle,
            modalContent: languageManager.loadedLanguage.profile.info.modalUnregister,
            modalYes: languageManager.loadedLanguage.common.button.modalYes,
            modalNo: languageManager.loadedLanguage.common.button.modalNo,
        },
        methods: {
            show() {
                modal.modalShow = true;
            },
            confirm(condition) {
                if(condition){
                    let ajax = new AjaxManager();
                    ajax.callback = () => {
                        let innerAjax = new AjaxManager();
                        innerAjax.callback = () => {
                            $(location).attr("href", "/");
                        };
                        innerAjax.getAjaxResponse("/login", "DELETE");
                    };
                    ajax.getAjaxResponse("/register", "DELETE");
                }
                modal.modalShow = false;
            }
        }
    });
    
    const profileIndicator = new Vue({
        el: ".body",
        data: {
            profileTitle: languageManager.loadedLanguage.profile.text.profileTitle,
            responseData: undefined,
            profileDataList: [
                {name: languageManager.loadedLanguage.common.text.id, tag: "user_id",
                    attributes: ["readonly", "noPassword", "noRegExr", "noRepeat", "noDBCheck"],
                    rule: undefined, error: undefined, input: "", warningSign: undefined,
                    allowedSign: undefined, response: "userId"},
                {name: languageManager.loadedLanguage.common.text.username, tag: "user_name",
                    attributes: ["notReadonly", "noPassword", "needRegExr", "noRepeat", "needDBCheck"],
                    rule: usernameRule, error: undefined, input: "", warningSign: ".warning_user_name",
                    allowedSign: ".allowed_user_name", response: "userName"},
                {name: languageManager.loadedLanguage.common.text.email, tag: "user_email",
                    attributes: ["notReadonly", "noPassword", "needRegExr", "noRepeat", "needDBCheck"],
                    rule: emailRule, error: undefined, input: "", warningSign: ".warning_user_email",
                    allowedSign: ".allowed_user_email", response: "userEmail"}
            ],
            profilePasswordList: [
                {name: languageManager.loadedLanguage.common.text.password, tag: "user_password",
                    attributes: ["notReadonly", "password", "needRegExr", "noRepeat", "noDBCheck"],
                    rule: passwordRule, error: undefined, warningSign: ".warning_user_password",
                    allowedSign: ".allowed_user_password", input: ""},
                {name: languageManager.loadedLanguage.common.text.passwordConfirm, tag: "user_password_confirm",
                    attributes: ["notReadonly", "password", "noRegExr", "repeat", "noDBCheck"],
                    rule: undefined, error: undefined, warningSign: ".warning_user_password_confirm",
                    allowedSign: ".allowed_user_password_confirm", input: ""}
            ],
            passwordMode: false,
            editMode: false,
            profileButtonList: [
                {name: languageManager.loadedLanguage.profile.button.changeInformation, tag: "button_changeInformation",
                    disabled: false, buttonLevel: "primary"},
                {name: languageManager.loadedLanguage.profile.button.changePassword, tag: "button_changePassword",
                    disabled: false, buttonLevel: "primary"},
                {name: languageManager.loadedLanguage.profile.button.unregister, tag: "button_unregister",
                    disabled: false, buttonLevel: "warning"},
                {name: languageManager.loadedLanguage.profile.button.confirmChange, tag: "button_confirmChange",
                    disabled: true, buttonLevel: "warning"},
                    {name: languageManager.loadedLanguage.profile.button.cancelChange, tag: "button_cancelChange",
                    disabled: true, buttonLevel: "primary"}
            ],
            elementLocked: false,
            loadLocked: false,
            indicatorList: [
                {url: "failure"},
                {url: "success"}
            ]
        },
        methods: {
            clickEvent(button) {
                if(button === "button_changeInformation") {
                    profileIndicator.editMode = true;
                }
                if(button === "button_changePassword") {
                    profileIndicator.passwordMode = true;
                }
                if(button === "button_unregister") {
                    modal.show();
                }
                if(button === "button_confirmChange") {
                    let queue;
                    let url = "/profile/";
                    if(profileIndicator.passwordMode) {
                        for(let i = 0; i < profileIndicator.profilePasswordList.length; i++) {
                            profileIndicator.inputEvent(profileIndicator.profilePasswordList[i], profileIndicator.profilePasswordList[i].input);
                        }
                        if(profileIndicator.profilePasswordList[0].error !== undefined) {
                            return false;
                        }
                        queue = JSON.stringify({
                            "userId": profileIndicator.responseData.userId,
                            "userPw": profileIndicator.profilePasswordList[0].input,
                        });
                        url += "password";
                    } else {
                        for(let i = 1; i < profileIndicator.profilePasswordList.length; i++) {
                            profileIndicator.inputEvent(profileIndicator.profileDataList[i], profileIndicator.profileDataList[i].input);
                            profileIndicator.changeEvent(profileIndicator.profilePasswordList[i], profileIndicator.profilePasswordList[i].input);
                        }
                        if(profileIndicator.profileDataList[1].error !== undefined || profileIndicator.profileDataList[2].error !== undefined) {
                            return false;
                        }
                        queue = JSON.stringify({
                            "userId": profileIndicator.responseData.userId,
                            "userName": profileIndicator.profileDataList[1].input,
                            "userEmail": profileIndicator.profileDataList[2].input,
                        });
                        url += "information";
                    }
                    let ajax = new AjaxManager();
                    ajax.callback = (data) => {
                        $(location).attr("href", data);
                    };
                    ajax.getAjaxResponse(url, "PATCH", queue, "text", "application/json");
                }
                if(button === "button_cancelChange") {
                    profileIndicator.editMode = false;
                    profileIndicator.passwordMode = false;
                }
                if(button !== "button_unregister"){
                    profileIndicator.profileButtonList.forEach((data) => {
                        data.disabled = !data.disabled;
                    });
                    if(!profileIndicator.loadLocked) {
                        let ajax = new AjaxManager();
                        ajax.callback = (data) => {
                            profileIndicator.responseData = data;
                            for (const dataKey in profileIndicator.responseData) {
                                if(profileIndicator.responseData[dataKey] == null) {
                                    delete profileIndicator.responseData[dataKey];
                                    continue;
                                }
                                for(const listKey in profileIndicator.profileDataList) {
                                    if(dataKey === profileIndicator.profileDataList[listKey].response) {
                                        profileIndicator.profileDataList[listKey].input = profileIndicator.responseData[dataKey];
                                    }
                                }
                            }
                        };
                        ajax.getAjaxResponse("/profile/data", "GET", null, "json");
                        profileIndicator.loadLocked = true;
                    }
                }
            },
            inputEvent(list, input) {
                list.error = undefined;
                $(".profile_input_" + list.tag).removeAttr("id");

                if(list.attributes[2] === "needRegExr" && list.rule !== undefined) {
                    if(!list.rule.test(input)) {
                        list.error = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.error.notSatisfied);
                    }
                }
                if(list.attributes[3] === "repeat" && profileIndicator.profilePasswordList[0].input !== input) {
                    list.error = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.error.notMatched);
                } else if(list.attributes[3] === "repeat" && profileIndicator.profilePasswordList[0].error !== undefined) {
                    list.error = messageManager.getMessage(profileIndicator.profilePasswordList[0].name, languageManager.loadedLanguage.common.error.notSatisfied);
                }
                if(list.error !== undefined) {
                    $(".profile_input_" + list.tag).attr("id", "gotEvent");
                    textManager.setText($(list.warningSign), list.error, false);
                    if(list.attributes[1] === "needRegExr" && list.requireType !== undefined) {
                        textManager.setText($(list.warningSign), languageManager.loadedLanguage.common.require[list.requireType], true);
                    }
                } else if (list.tag === "user_password") {
                    $(".profile_input_" + list.tag).attr("id", "gotEvent");
                    textManager.setText(list.allowedSign, languageManager.loadedLanguage.common.info.passed, false);
                } else if (list.tag === "user_password_confirm") {
                    $(".profile_input_" + list.tag).attr("id", "gotEvent");
                    const message = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.info.matched);
                    textManager.setText(list.allowedSign, message, false);
                }
            },
            changeEvent(list, input) {
                if (list.attributes[4] === "needDBCheck" && input !== "" && list.error === undefined) {
                    let ajax = new AjaxManager();
                    ajax.callback = (resultData) => {
                        $(".profile_input_" + list.tag).attr("id", "gotEvent");
                        if (!resultData.condition || resultData.condition === undefined) {
                            list.error = messageManager.getMessage(list.name, languageManager.loadedLanguage.common.error.existed);
                            textManager.setText($(list.warningSign), list.error, false);
                        } else {
                            textManager.setText(list.allowedSign, languageManager.loadedLanguage.common.info.passed, false);
                        }
                    };
                    ajax.getAjaxResponse("/register/check", "GET", null, "json", "application/json", (xhr) => {
                        xhr.setRequestHeader('registerCheck', JSON.stringify({"type": list.tag, "content": input}));
                    });
                }
            }
        },
        mounted: function () {
            let ajax = new AjaxManager();
            ajax.callback = (data) => {
                this.responseData = data;
                for (const dataKey in this.responseData) {
                    if(this.responseData[dataKey] == null) {
                        delete this.responseData[dataKey];
                        continue;
                    }
                    for(const listKey in this.profileDataList) {
                        if(dataKey === this.profileDataList[listKey].response) {
                            this.profileDataList[listKey].input = this.responseData[dataKey];
                        }
                    }
                }
            };
            ajax.getAjaxResponse("/profile/data", "GET", null, "json");
            if(currentURL[4] !== "" && currentURL[4] !== undefined) {
                if(currentURL[4] === this.indicatorList[0].url){
                    textManager.setText($(".profileNotifierViewer"), languageManager.loadedLanguage.profile.error.error, false);
                    $(".profileNotifier").removeAttr("id");
                } else if(currentURL[4] === this.indicatorList[1].url) {
                    textManager.setText($(".profileNotifierViewer"), languageManager.loadedLanguage.profile.info.changed, false);
                    $(".profileNotifier").removeAttr("id");
                }
            }
        },
        updated: function () {
            if(!profileIndicator.elementLocked){
                profileIndicator.profileDataList.forEach((data) => {
                    if(data.attributes[0] === "readonly") {
                        $(".profile_input_" + data.tag).attr("readonly", "");
                    }
                });
                profileIndicator.profilePasswordList.forEach((data) => {
                    if(data.attributes[1] === "password") {
                        $(".profile_input_" + data.tag).attr("type", "password");
                    }
                });
                profileIndicator.elementLocked = true;
            }
        },
    });
});