// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    Vue.component('replier', (resolve, reject) => {
        let ajax = new AjaxManager();
        ajax.callback = (data) => {
            resolve({
                template: data,
                props: ["enabled", "parentIndex", "description", "confirmButton", "cancelButton"],
                data: function() {
                    return {
                        commentContents : ""
                    }
                },
                methods: {
                    replierClickEvent(button, parentIndex, commentContents) {
                        this.$emit("replier-click-event", button, parentIndex, commentContents);
                    }
                }
            })
        };
        ajax.getAjaxResponse("/html/replier.html", "GET");
    });

    const modal = new Vue({
        el: '.modalIndicator',
        data: {
            modalShow: false,
            modalHeader: languageManager.loadedLanguage.lecture.info.modalTitle,
            modalContent: languageManager.loadedLanguage.lecture.info.modalRemoveOne,
            modalYes: languageManager.loadedLanguage.common.button.modalYes,
            modalNo: languageManager.loadedLanguage.common.button.modalNo,
            postNumber: undefined,
        },
        methods: {
            show() {
                modal.modalShow = true;
            },
            confirm(condition) {
                if(condition){
                    let ajax = new AjaxManager();
                    ajax.callback = () => {
                        $(location).attr("href", "/lectures");
                    };
                    ajax.getAjaxResponse("/lectures/" + modal.postNumber, "DELETE");
                }
                modal.modalShow = false;
            }
        }
    });

    const body = new Vue({
        el: ".body",
        data: {
            userId: undefined,
            userPermission: undefined,
            lectureEditorLoaded: false,
            lectureContents: [],
            lectureOriginalContents: undefined,
            lecturePageLanguage: $.cookie("languagePreference"),
            lecturePageIndex: 0,
            lectureEndOfList: false,
            lectureConfirmPost: languageManager.loadedLanguage.common.button.confirmPost,
            lectureTitle: languageManager.loadedLanguage.common.text.title,
            lectureContentId: null,
            lectureContentTitle: undefined,
            lectureContentDirectory: Math.floor(Math.random() * 1000000000000000000) + 1,
            lectureContentLanguage: $.cookie("languagePreference"),
            lectureContentEditMode: false,
            lectureContentEditId: undefined,
            lectureContentEditTitle: undefined,
            lectureContentEditContent: undefined,
            adminButton: [
                {name: languageManager.loadedLanguage.common.button.deletePost, tag: "delete_post", class: "danger"},
                {name: languageManager.loadedLanguage.common.button.editPost, tag: "edit_post", class: "primary"},
                {name: languageManager.loadedLanguage.common.button.confirmPost, tag: "confirm_post", class: "danger"},
                {name: languageManager.loadedLanguage.common.button.cancelPost, tag: "cancel_post", class: "primary"},
            ],
            lectureCommentData: [],
            lectureCommentEditMode: [],
            lectureEndOfLectures: false,
            lectureEndOfComments: false,
            lectureCommentRequiredLogin: languageManager.loadedLanguage.common.require.commentLogin,
            lectureCommentSection: languageManager.loadedLanguage.common.info.commentHere,
            lectureCommentConfirm: languageManager.loadedLanguage.common.button.confirmComment,
            commentButton: [
                {name: languageManager.loadedLanguage.common.button.editComment, class: "primary", tag: "Edit_comment"},
                {name: languageManager.loadedLanguage.common.button.deleteComment, class: "danger", tag: "Delete_comment"},
                {name: languageManager.loadedLanguage.common.button.confirmComment, class: "danger", tag: "Confirm_edit_comment"},
                {name: languageManager.loadedLanguage.common.button.cancelComment, class: "danger", tag: "Cancel_edit_comment"},
            ],
            lectureCommentShow: languageManager.loadedLanguage.lecture.button.showComment,
            lectureCommentHide: languageManager.loadedLanguage.lecture.button.hideComment,
            commentManager: undefined,
            dateManager: undefined,
            lectureContinueButton: languageManager.loadedLanguage.lecture.button.continue,
            lectureSearch: languageManager.loadedLanguage.common.text.search,
            lectureSearchValue: undefined,
            lectureSearchMode: false,
        },
        methods: {
            clickEvent(button, index) {
                if(button.tag === "edit_post") {
                    $(".lectureEditor").summernote("destroy");
                    body.lectureContentEditMode = true;
                    body.lectureContents[index]["lectureContentEditMode"] = true;
                    body.lectureContentEditTitle = $(".lectureContentTitle_" + index).text();
                    body.lectureContentEditContent = $(".lectureContentList_" + index).html();
                    body.$nextTick(() => {
                        $(".lectureContentList_EditMode_" + index).summernote({
                            codeviewFilter: true,
                            callbacks: {
                                width: 732,
                                onImageUpload: function(files, editor, welEditable) {
                                    vm.imageEvent(files, editor, welEditable);
                                }
                            }
                        });
                        $(".lectureContentList_EditMode_" + index).summernote('code', body.lectureContentEditContent);
                    });
                } else if(button.tag === "delete_post") {
                    modal.postNumber = body.lectureContents[index]["lectureId"];
                    modal.show();
                } else if(button.tag === "confirm_post") {
                    body.editorClickEvent(true, index);
                } else if(button.tag === "cancel_post") {
                    body.lectureContentEditMode = false;
                    body.lectureContents[index]["lectureContentEditMode"] = false;
                    $(".lectureContentList_EditMode_" + index).summernote("destroy");
                    body.$nextTick(() => {
                        $(".lectureEditor").summernote({
                            codeviewFilter: true,
                            callbacks: {
                                width: 732,
                                onImageUpload: function(files, editor, welEditable) {
                                    vm.imageEvent(files, editor, welEditable);
                                }
                            }
                        });
                    });
                }
            },
            editorClickEvent(isEditMode, index) {
                let vm = body;
                let ajax = new AjaxManager();
                let queue;
                if(!isEditMode) {
                    let contentData = $(".lectureEditor").summernote('code');
                    if(vm.lectureContentTitle === undefined && contentData === undefined) {
                        return false;
                    }
                    queue = JSON.stringify({
                        "lectureTitle": vm.lectureContentTitle,
                        "lectureContents": contentData,
                        "lectureDirectory": vm.lectureContentDirectory,
                        "lectureLanguage": vm.lectureContentLanguage
                    });
                } else {
                    if(body.lectureContentEditTitle === undefined && body.lectureContentEditContent === undefined) {
                        return false;
                    }
                    body.lectureContentEditId = body.lectureContents[index]["lectureId"];
                    body.lectureContentEditContent = $(".note-editable").html();
                    queue = JSON.stringify({
                        "lectureId": body.lectureContentEditId,
                        "lectureTitle": body.lectureContentEditTitle,
                        "lectureContents": body.lectureContentEditContent,
                        "lectureDirectory": body.lectureContents[index]["lectureDirectory"],
                        "lectureLanguage": body.lectureContents[index]["lectureLanguage"],
                    });
                }
                ajax.callback = () => {
                    $(location).attr("href", "");
                };
                if(!isEditMode) {
                    ajax.getAjaxResponse("/lectures", "POST", queue, "text", "application/json");
                } else {
                    ajax.getAjaxResponse("/lectures/" + body.lectureContentEditId, "PATCH", queue, "text", "application/json");
                }
            },
            imageEvent(files) {
                body.uploadImage(files, body, "/lectures/file");
            },
            uploadImage(files, vm, url) {
                if(files != null) {
                    let transferFiles = new FormData();
                    for(let i = 0; i < files.length; i++ ) {
                        if(files[i].size / 1024 / 1024 > 480) {
                            alert(languageManager.loadedLanguage.common.error.exceedSizeLimitation);
                            return false;
                        }
                        transferFiles.append('file', files[i]);
                    }
                    transferFiles.append('directory', vm.lectureContentDirectory);
                    let ajax = new AjaxManager();
                    ajax.callback = (data) => {
                        if(data !== "failure") {
                            for(let i = 0; i < data.length; i++) {
                                $(".lectureEditor").summernote('editor.insertImage', data[i]);
                            }
                        }
                    };
                    ajax.getAjaxResponse(url, "POST", transferFiles, "json", false, false, false);
                }
            },
            showDetail(index) {
                if(body.lectureContents[index].lectureShowDetail) {
                    body.lectureContents[index].lectureShowDetail = false;
                    $(".lectureContentDetail_" + index).removeAttr("id");
                } else {
                    body.lectureContents[index].lectureShowDetail = true;
                    $(".lectureContentDetail_" + index).attr("id", "detailsShown");
                    body.pageEvent(index);
                }
            },
            loadLectureList() {
                if(!body.lectureEndOfLectures || body.lectureSearchValue) {
                    let ajax = new AjaxManager();
                    ajax.callback = (data) => {
                        for(let i = 0; i < data.length; i++) {
                            data[i]["lectureShowDetail"] = false;
                            data[i]["lectureContentEditMode"] = false;
                            data[i]["lectureCommentIndex"] = 0;
                        }
                        body.dateManager.data = data;
                        body.dateManager.type = "lecture";
                        const replicatedData = body.dateManager.getDate();
                        replicatedData.forEach((element, index) => {
                            Vue.set(body.lectureCommentEditMode, index, {enabled: false});
                        });
                        body.lectureOriginalContents = replicatedData;
                        body.lectureContents.push(...replicatedData);
                        body.lecturePageIndex++;
                        if(data.length < 5) {
                            body.lectureEndOfLectures = true;
                        }
                    };
                    ajax.getAjaxResponse("lectures/list", "GET", null, "json", null, (xhr) => {
                        xhr.setRequestHeader("PageLanguage", body.lecturePageLanguage);
                        xhr.setRequestHeader("PageIndex", body.lecturePageIndex * 5);
                    });
                }
            },
            pageEvent(tableId) {
                const queue = JSON.stringify({
                    targetTable: "lecture",
                    targetTableId: body.lectureContents[tableId].lectureId,
                    index: body.lectureContents[tableId].lectureCommentIndex * 10
                });
                let ajax = new AjaxManager();
                ajax.callback = (data) => {
                    body.dateManager.data = data;
                    body.dateManager.type = "comment";
                    const replicatedData = body.dateManager.getDate();
                    replicatedData.forEach((element, index) => {
                        Vue.set(body.lectureCommentEditMode, index, {enabled: false});
                    });
                    body.lectureCommentData.push(...replicatedData);
                    if(data.length < 15) {
                        body.lectureEndOfComments = true;
                    }
                };
                ajax.getAjaxResponse("/comments/" + body.lectureContents[tableId].lectureId, "GET", null, "json", null, (xhr) => {
                    body.lectureContents[tableId].lectureCommentIndex++;
                    xhr.setRequestHeader("Comment", queue);
                });
            },
            searchEvent() {
                body.lectureSearchMode = true;
                if(body.lectureSearchValue === "" || body.projectSearchValue === undefined) {
                    body.lectureSearchMode = false;
                    body.lectureContents = body.lectureOriginalContents;
                    return true;
                }
                let ajax = new AjaxManager();
                ajax.callback = (data) => {
                    body.lectureContents = [];
                    if(data !== "" && data !== undefined) {
                        body.dateManager.data = data;
                        body.dateManager.type = "lecture";
                        const replicatedData = body.dateManager.getDate();
                        body.lectureContents = JSON.parse(replicatedData);
                    }
                };
                ajax.getAjaxResponse("/lectures/" + body.lectureSearchValue, "GET", null, "text", "application/x-form-urlencoded", (request) => {
                    request.setRequestHeader("pageLanguage", body.lecturePageLanguage);
                    request.setRequestHeader("searchValue", encodeURIComponent(body.lectureSearchValue));
                });
            },
            commentClickEvent(button, index, commentId) {
                if(button === "leave"){
                    body.commentManager.commentLeaveEvent($(".lectureCommentReplyText_" + index).val(), "lecture", body.lectureContents[index].lectureId);
                } else if(button === "edit") {
                    body.commentManager.commentEditEvent(body.lectureCommentEditMode, index);
                } else if(button === "delete") {
                    body.commentManager.commentDeleteEvent(body.lectureCommentData, index, body.lectureCommentData[index]["commentId"], body.lectureCommentData[index]["commentId"]);
                } else if(button === "confirm") {
                    body.commentManager.commentConfirmEditEvent(body.lectureCommentEditMode, $(".lectureCommentEditText_" + index).val(), index, commentId, body.lectureContents[index].lectureId);
                } else if (button === "cancel") {
                    body.commentManager.commentCancelEditEvent(body.lectureCommentEditMode, index);
                }
            },
        },
        mounted: function() {
            let vm = this;
            this.commentManager = new CommentManager();
            this.dateManager = new DateManager();
            let ajax = new AjaxManager();
            ajax.callback = (data, textStatus, response) => {
                vm.userId = response.getResponseHeader("Id");
            };
            ajax.getAjaxResponse("/id", "HEAD", null, "text", null, null);
            let ajax2 = new AjaxManager();
            ajax2.success = (data, textStatus, response) => {
                this.userPermission = response.getResponseHeader("Permission");
                this.loadLectureList();
            };
            ajax2.isValid("/permission", "HEAD", "Permission");
        },
        updated() {
            let vm = body;
            if(!vm.lectureEditorLoaded) {
                vm.lectureEditorLoaded = true;
                $(".lectureEditor").summernote({
                    codeviewFilter: true,
                    callbacks: {
                        width: 732,
                        onImageUpload: function(files, editor, welEditable) {
                            vm.imageEvent(files, editor, welEditable);
                        }
                    }
                });
            }
        },
        computed: {
            lectureCommentDataLength() {
                if(this.lectureCommentData !== undefined) {
                    return messageManager.getMessage(this.lectureCommentData.length, languageManager.loadedLanguage.common.info.totalComments);
                }
            }
        },
    });
});