// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    const modal = new Vue({
        el: '.modalIndicator',
        data: {
            modalShow: false,
            modalHeader: languageManager.loadedLanguage.project.info.modalTitle,
            modalContent: languageManager.loadedLanguage.project.info.modalRemoveOne,
            modalYes: languageManager.loadedLanguage.common.button.modalYes,
            modalNo: languageManager.loadedLanguage.common.button.modalNo,
        },
        methods: {
            show() {
                modal.modalShow = true;
            },
            confirm(condition) {
                if(condition){
                    let ajax = new AjaxManager();
                    ajax.callback = () => {
                        $(location).attr("href", "/projects");
                    };
                    ajax.getAjaxResponse("/projects/" + currentURL[4], "DELETE");
                }
                modal.modalShow = false;
            }
        }
    });

    const body = new Vue({
        el: ".body",
        data: {
            userPermission: undefined,
            userId: undefined,
            projectAuthor: languageManager.loadedLanguage.common.text.author,
            projectContentTitle: undefined,
            projectContentSubtitle: undefined,
            projectContentData: undefined,
            projectContentAuthor: undefined,
            projectContentFile: undefined,
            adminButton: [
                {name: languageManager.loadedLanguage.common.button.editPost, tag: "primary", url: "/projects/" + currentURL[4] + "/edit"},
                {name: languageManager.loadedLanguage.common.button.deletePost, tag: "danger", url: "/projects" + currentURL[4]},
            ],
            projectCommentData: [],
            projectCommentIndex: 0,
            projectEndOfComments: false,
            projectCommentRequiredLogin: languageManager.loadedLanguage.common.require.commentLogin,
            projectCommentSection: languageManager.loadedLanguage.common.info.commentHere,
            projectCommentConfirm: languageManager.loadedLanguage.common.button.confirmComment,
            projectCommentTotal: "",
            commentManager: undefined,
            dateManager: undefined,
            commentButton: [
                {name: languageManager.loadedLanguage.common.button.editComment, class: "primary", tag: "Edit_comment"},
                {name: languageManager.loadedLanguage.common.button.deleteComment, class: "danger", tag: "Delete_comment"},
                {name: languageManager.loadedLanguage.common.button.confirmComment, class: "danger", tag: "Confirm_edit_comment"},
                {name: languageManager.loadedLanguage.common.button.cancelComment, class: "danger", tag: "Cancel_edit_comment"},
            ],
            projectCommentEditMode: [],
            projectCommentReplyMode: [],
        },
        methods: {
            clickEvent(button) {
                if(button.url === "/projects/" + currentURL[4] + "/edit") {
                    $(location).attr("href", button.url);
                } else if(button.url === "/projects" + currentURL[4]) {
                    modal.show();
                }
            },
            pageEvent() {
                if(!body.projectEndOfComments) {
                    const queue = JSON.stringify({
                        targetTable: "project",
                        targetTableId: currentURL[4],
                        index: body.projectCommentIndex * 10
                    });
                    let ajax = new AjaxManager();
                    ajax.callback = (data) => {
                        body.dateManager.data = data;
                        body.dateManager.type = "comment";
                        const replicatedData = body.dateManager.getDate();
                        replicatedData.forEach((element, index) => {
                            Vue.set(body.projectCommentEditMode, index, {enabled: false});
                        });
                        body.projectCommentData.push(...replicatedData);
                        if(data.length < 15) {
                            body.projectEndOfComments = true;
                        }
                    };
                    ajax.getAjaxResponse("/comments/" + currentURL[4], "GET", null, "json", null, (xhr) => {
                        body.projectCommentIndex++;
                        xhr.setRequestHeader("Comment", queue);
                    });
                }
            },
            commentClickEvent(button, index, commentId) {
                if(button === "leave"){
                    body.commentManager.commentLeaveEvent($(".projectCommentReplyText").val(), "project", currentURL[4]);
                } else if(button === "edit") {
                    body.commentManager.commentEditEvent(body.projectCommentEditMode, index);
                } else if(button === "delete") {
                    body.commentManager.commentDeleteEvent(body.projectCommentData, index, body.projectCommentData[index]["commentId"], currentURL[4]);
                } else if(button === "confirm") {
                    body.commentManager.commentConfirmEditEvent(body.projectCommentEditMode, $(".projectCommentEditText_" + index).val(), index, commentId, currentURL[4]);
                } else if (button === "cancel") {
                    body.commentManager.commentCancelEditEvent(body.projectCommentEditMode, index);
                }
            },
        },
        computed: {
            projectCommentDataLength() {
                if(this.projectCommentData !== undefined) {
                    return messageManager.getMessage(this.projectCommentData.length, languageManager.loadedLanguage.common.info.totalComments);
                }
            }
        },
        mounted: function() {
            const vm = this;
            let ajax = new AjaxManager();
            this.commentManager = new CommentManager();
            this.dateManager = new DateManager();
            ajax.callback = (data) => {
                this.projectContentTitle = data["projectTitle"];
                this.projectContentSubtitle = data["projectSubtitle"];
                this.projectContentData = data["projectContents"];
                this.projectContentAuthor = data["projectAuthor"];
                if(data["projectFile"] !== undefined) {
                    let temp = JSON.parse(data["projectFile"]);
                    if(temp !== "" && temp !== undefined && temp !== null) {
                        for(let i = 0; i < temp.length; i++) {
                            const croppedURL = temp[i].split("/");
                            temp[i] = {
                                "file": croppedURL,
                                "name": croppedURL[croppedURL.length - 1]
                            };
                        }
                        this.projectContentFile = temp;
                    }
                }
                let innerAjax1 = new AjaxManager();
                innerAjax1.callback = (data, textStatus, response) => {
                    vm.userId = response.getResponseHeader("Id");
                };
                innerAjax1.getAjaxResponse("/id", "HEAD", null, "text", null, null);
                let innerAjax2 = new AjaxManager();
                innerAjax2.callback = (data, status ,response) => {
                    vm.userPermission = response.getResponseHeader("Permission");
                };
                innerAjax2.getAjaxResponse("/permission", "HEAD", null, "text", null, null);
            };
            ajax.getAjaxResponse("/projects/" + currentURL[4] + "/contents", "GET", null, "json", null, (xhr) => {
                xhr.setRequestHeader("projectId", currentURL[4]);
            });
        }
    });

    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() === $(document).height()) {
            if(!body.projectEndOfComments) {
                body.pageEvent();
            }
        }
    });
});