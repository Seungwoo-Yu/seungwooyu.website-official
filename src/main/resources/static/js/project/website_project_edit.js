// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    const body = new Vue({
        el: ".body",
        data: {
            userPermission: undefined,
            projectSubtitle: languageManager.loadedLanguage.project.text.editSubtitle,
            projectTitleDescription: languageManager.loadedLanguage.common.text.title,
            projectSubtitleDescription: languageManager.loadedLanguage.project.text.subtitleDescription,
            projectVideoTargetDescription: languageManager.loadedLanguage.project.text.target,
            projectVideoDescription: languageManager.loadedLanguage.project.text.videoUpload,
            projectFileDescription: languageManager.loadedLanguage.project.text.fileUpload,
            projectUploadingWarning: languageManager.loadedLanguage.common.info.warning,
            projectUploadingIndicator: "0%",
            projectUploading: false,
            projectFileReset: false,
            postButton: [
                {name: languageManager.loadedLanguage.common.button.confirmPost, url: "/projects/" + currentURL[4]},
                {name: languageManager.loadedLanguage.common.button.cancelPost, url: "/projects"},
            ],
            projectContentId: undefined,
            projectContentTitle: undefined,
            projectContentSubtitle: undefined,
            projectContentVideoURL: undefined,
            projectContentVideoLocation: undefined,
            projectContentFileURL: undefined,
            projectContentDirectory: undefined,
            projectContentLanguage: $.cookie("languagePreference"),
        },
        methods: {
            clickEvent(button) {
                if(button.url === "/projects/" + currentURL[4]){
                    const compressedQueue = compileProject(true, body);
                    let ajax = new AjaxManager();
                    ajax.callback = () => {
                        $(location).attr("href", "/projects/" + currentURL[4]);
                    };
                    ajax.getAjaxResponse(button.url, "PATCH", compressedQueue, "text", "application/json");
                } else if(button.url === "/projects") {
                    $(location).attr("href", button.url);
                }
            },
            imageEvent(files) {
                uploadImage(files, body, "/projects/file");
            },
            uploadEvent(className) {
                uploadFile(className, body, "/projects/file");
            },
            xhrListener(vm) {
                let xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    vm.projectUploading = true;
                    if (evt.lengthComputable) {
                        vm.projectUploadingIndicator = Math.floor((evt.loaded / evt.total) * 100) + "%";
                    }
                }, false);
                return xhr;
            },
            fileExceedWarning(reset) {
                fileWarning(reset, body);
            }
        },
        mounted: function() {
            let vm = this;
            let ajax = new AjaxManager();
            ajax.callback = (data) => {
                this.projectContentId = data["projectId"];
                this.projectContentTitle = data["projectTitle"];
                this.projectContentSubtitle = data["projectSubtitle"];
                $(".projectEditor").summernote('code', data["projectContents"]);
                this.projectContentFileURL = data["projectContentFile"];
                this.projectContentDirectory = data["projectDirectory"];
                this.projectContentLanguage = data["projectLanguage"];
                ajax.failure = () => {
                    $(location).attr("href", "/");
                };
                ajax.success = (data, textStatus, response) => {
                    this.userPermission = response.getResponseHeader("Permission");
                    if(this.userPermission !== undefined && this.userPermission !== "ADMIN") {
                        $(location).attr("href", "/");
                    }
                };
                ajax.isValid("/permission", "HEAD", "Permission");
            };
            ajax.getAjaxResponse("/projects/" + currentURL[4] + "/contents", "GET", null, "json", null, (xhr) => {
                xhr.setRequestHeader("projectId", currentURL[4]);
            });
            $(".projectEditor").summernote({
                codeviewFilter: true,
                callbacks: {
                    width: 732,
                    onImageUpload: function(files, editor, welEditable) {
                        vm.imageEvent(files, editor, welEditable);
                    }
                }
            });
        }
    });
});