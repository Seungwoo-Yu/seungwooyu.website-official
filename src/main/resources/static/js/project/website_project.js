// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    const body = new Vue({
        el: ".body",
        data: {
            userPermission: undefined,
            projectContents: undefined,
            projectOriginalContents: undefined,
            projectPageLanguage: $.cookie("languagePreference"),
            projectPageIndex: 0,
            projectDeleteMode: false,
            projectDeleteList: [],
            projectEndOfList: false,
            adminButton: [
                {name: languageManager.loadedLanguage.common.button.addPost, tag: "add_post", class: "primary", url: "/projects/post"},
                {name: languageManager.loadedLanguage.common.button.deletePost, tag: "delete_post", class: "danger", url: "/projects"},
                {name: languageManager.loadedLanguage.common.button.confirmPost, tag: "confirm_post", class: "danger", url: undefined},
                {name: languageManager.loadedLanguage.common.button.cancelPost, tag: "cancel_post", class: "primary", url: undefined},
            ],
            projectDefaultImage: "https://ec2-hdd.s3.ap-northeast-2.amazonaws.com/no-image-selected.png",
            projectSearch: languageManager.loadedLanguage.common.text.search,
            projectSearchValue: undefined,
            projectSearchMode: false,
        },
        methods: {
            clickEvent(button) {
                if(button.tag === "add_post") {
                    $(location).attr("href", button.url);
                } else if(button.tag === "delete_post") {
                    body.projectDeleteMode = true;
                } else if(button.tag === "confirm_post") {
                    let selectedList = body.projectDeleteList;
                    let sortedList = [];
                    let contents = body.projectContents;
                    selectedList.forEach(element => {
                        if(element !== undefined) {
                            sortedList.push(contents[element].projectId);
                        }
                    });
                    let ajax = new AjaxManager();
                    ajax.callback = () => {
                        $(location).attr("href", "/projects");
                    };
                    ajax.getAjaxResponse("/projects", "DELETE", JSON.stringify(sortedList), "text", "application/json");
                    body.projectDeleteList = [];
                    body.projectDeleteMode = false;
                } else if(button.tag === "cancel_post") {
                    body.projectDeleteList = [];
                    body.projectDeleteMode = false;
                }
            },
            postEvent(index, postNumber) {
                if(!body.projectDeleteMode) {
                    $(location).attr("href", "/projects/" + postNumber);
                } else {
                    let isExisted = [false, undefined];
                    let list = body.projectDeleteList;
                    for(i = 0; i < list.length; i++) { 
                        if(index === list[i]) {
                            isExisted = [true, i];
                            break;
                        }
                    }
                    if(isExisted[0]) {
                        $(".projectList_" + index).removeAttr("id");
                        delete list[isExisted[1]];
                    } else {
                        $(".projectList_" + index).attr("id", "projectSelected");
                        list.push(index);
                    }
                }
            },
            pageEvent() {
                if(!body.projectEndofList && !body.projectSearchMode) {
                    let ajax = new AjaxManager();
                    ajax.callback = (data) => {
                        body.projectContents.push(...data);
                        if(data.length === 0) {
                            body.projectEndofList = true;
                        }
                    };
                    ajax.getAjaxResponse("/projects/list", "GET", null, "json", null, (request) => {
                        body.projectPageIndex++;
                        request.setRequestHeader("pageLanguage", this.projectPageLanguage);
                        request.setRequestHeader("pageIndex", this.projectPageIndex * 10);
                    });
                }
            },
            searchEvent() {
                body.projectSearchMode = true;
                if(body.projectSearchValue === "" || body.projectSearchValue === undefined) {
                    body.projectSearchMode = false;
                    body.projectContents = body.projectOriginalContents;
                    return true;
                }
                let ajax = new AjaxManager();
                ajax.callback = (data) => {
                    body.projectContents = [];
                    if(data !== "" && data !== undefined) {
                        body.projectContents = JSON.parse(data);
                    }
                };
                ajax.getAjaxResponse("/projects/" + body.projectSearchValue, "GET", null, "text", "application/x-form-urlencoded", (request) => {
                    request.setRequestHeader("pageLanguage", body.projectPageLanguage);
                    request.setRequestHeader("searchValue", encodeURIComponent(body.projectSearchValue));
                });
            }
        },
        mounted: function() {
            let ajax = new AjaxManager();
            ajax.callback = (data) => {
                this.projectContents = data;
                const protectedContents = data;
                this.projectOriginalContents = protectedContents;
                ajax.callback = (data, status, xhr) => {
                    this.userPermission = xhr.getResponseHeader("Permission");
                };
                ajax.getAjaxResponse("/permission", "HEAD", null, "text", null, null);
            };
            ajax.getAjaxResponse("/projects/list", "GET", null, "json", null, (request) => {
                request.setRequestHeader("pageLanguage", this.projectPageLanguage);
                request.setRequestHeader("pageIndex", this.projectPageIndex * 10);
            });
        },
    });

    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() === $(document).height()) {
            body.pageEvent();
        }
     });
});