function compileProject(isEdit, vm) {
    if(vm.projectContentTitle !== undefined && vm.projectContentSubtitle !== undefined) {
        let contentData = $(".projectEditor").summernote('code');
        if(contentData !== undefined && contentData !== "" && $(contentData).text() !== "") {
            const data = contentData.split(vm.videolocation);
            if(data !== undefined && vm.projectContentVideoURL !== undefined) {
                contentData = "";
                if(data[0] !== undefined) {
                    contentData += data[0];
                }
                contentData += "<video controls><source src='" + vm.projectContentVideoURL + "'></source></video>";
                if(data[1] !== undefined) {
                    contentData += data[1];
                }
            }
            let firstImageParser = "<img";
            let initialLocation = contentData.indexOf(firstImageParser);
            let imageURL = 'src="';
            let start = contentData.indexOf(imageURL, (initialLocation + firstImageParser.length));
            let end = contentData.indexOf('"', (start + imageURL.length + 1));
            let firstImageURL;
            if(start > -1 && end > -1) {
                firstImageURL = contentData.substring(start + imageURL.length, end);
            }
            const queue = {
                "projectTitle": vm.projectContentTitle,
                "projectSubtitle": vm.projectContentSubtitle,
                "projectContents": contentData,
                "projectFile": vm.projectContentFileURL,
                "projectDirectory": vm.projectContentDirectory,
                "projectLanguage": vm.projectContentLanguage
            };
            if(firstImageURL !== undefined) {
                queue.projectThumbnail = firstImageURL;
            }
            if(isEdit) {
                queue.projectId = vm.projectContentId;
            }
            return JSON.stringify(queue);
        }
    }
}
function uploadImage(files, vm, url) {
    if(files != null) {
        let transferFiles = new FormData();
        for(let i = 0; i < files.length; i++ ) {
            if(files[i].size / 1024 / 1024 > 480) {
                vm.fileExceedWarning(false);
                return false;
            }
            transferFiles.append('file', files[i]);
        }
        transferFiles.append('directory', vm.projectContentDirectory);
        let ajax = new AjaxManager();
        ajax.callback = (data) => {
            vm.projectUploading = false;
            if(data !== "failure") {
                for(let i = 0; i < data.length; i++) {
                    $(".projectEditor").summernote('editor.insertImage', data[i]);
                }
            }
        };
        ajax.getAjaxResponse(url, "POST", transferFiles, "json", false, false, false, () => {
            return vm.xhrListener(vm);
        });
    }
}
function uploadFile(className, vm, url) {
    if($("."+ className)[0].files[0] != null) {
        let transferFiles = new FormData();
        if(className === "projectVideo") {
            if($("."+ className)[0].files[0].size / 1024 / 1024 > 480) {
                vm.fileExceedWarning(true);
                return false;
            }
            transferFiles.append('file', $("."+ className)[0].files[0]);
        } else {
            for(let i = 0; i < $("."+ className)[0].files.length; i++ ) {
                if($("."+ className)[0].files[i].size / 1024 / 1024 > 480) {
                    vm.fileExceedWarning(true);
                    return false;
                }
                transferFiles.append('file', $("."+ className)[0].files[i]);
            }
        };
        transferFiles.append('directory', vm.projectContentDirectory);
        let ajax = new AjaxManager();
        ajax.callback = (data) => {
            vm.projectUploading = false;
            if(className === "projectVideo") {
                vm.projectContentVideourl = data[0];
            } else {
                vm.projectContentFileurl = data;
            }
        };
        ajax.getAjaxResponse(url, "POST", transferFiles, "json", false, false, false, () => {
            return vm.xhrListener(vm);
        });
    }
}
function fileWarning(reset, vm) {
    if(reset) {
        vm.projectFileReset = true;
        vm.$nextTick(() => {
            vm.projectFileReset = false;
        })
    }
    alert(languageManager.loadedLanguage.common.error.exceedSizeLimitation);
}