// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
/*
    Vue.js is not re-mounted onto HTML DOM as Angular.js 1.7.8 (Deprecated version for javascript only).
    Therefore, it is required to be loaded when Document and all Elements on Document are ready.
*/
$(document).ready(() => {
    
});