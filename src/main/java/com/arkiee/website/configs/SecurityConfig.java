// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.configs;

import com.arkiee.website.filters.CSRFFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.csrf.CsrfFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.GET, "/resources/**", "/", "/about",
                                                    "/projects", "/error","/error/**",
                                                    "/login", "/login/**", "/register",
                                                    "/register/**", "/profile", "/minified")
                        .antMatchers(HttpMethod.POST, "/login/**", "/register/**", "/projects/**", "/comments/{\\d+}", "/comments/{\\d+}/replies/{\\d+}", "/lectures", "/lectures/{\\d+}", "/lectures/file")
                        .antMatchers(HttpMethod.DELETE, "/login", "/register", "/projects", "/projects/{\\d+}", "/comments/{\\d+}", "/lectures/{\\d+}")
                        .antMatchers(HttpMethod.PATCH, "/projects/{\\d+}", "/comments/{\\d+}", "/profile/password", "/profile/information", "/lectures/{\\d+}");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().disable();
        http.csrf().disable().addFilterBefore(new CSRFFilter(), CsrfFilter.class);
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).maximumSessions(1);
    }
}