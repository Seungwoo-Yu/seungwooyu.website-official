// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.configs;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AmazonS3Config {
    @Value("${server.amazon.key-id}")
    private String keyId;

    @Value("${server.amazon.secret-access-key}")
    private String secretKey;

    @Value("${server.amazon.bucket-name}")
    private String bucketName;

    @Bean
    public BasicAWSCredentials credentials() {
        return new BasicAWSCredentials(keyId, secretKey);
    }

    @Bean
    public AmazonS3 createConnectionWithCredentials() {
        return AmazonS3ClientBuilder.standard()
                                    .withCredentials(new AWSStaticCredentialsProvider(credentials()))
                                    .withRegion(Regions.AP_NORTHEAST_2)
                                    .build();
    }

    @Bean("bucketName")
    public String bucketName() {
        return bucketName;
    }
}