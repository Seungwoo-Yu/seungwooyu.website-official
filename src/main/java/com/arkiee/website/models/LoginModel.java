// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models;

import org.springframework.stereotype.Repository;

@Repository
public class LoginModel {
    private String userId;
    private String userPw;
    private boolean enableCookie;

    public LoginModel() {}

    public LoginModel(String userId, String userPw, boolean enableCookie) {
        this.userId = userId;
        this.userPw = userPw;
        this.enableCookie = enableCookie;
    }

    /*
        Getter for User Id in this model
        @param None
        @return UserId
    */
    public String getUserId() {
        return this.userId;
    }

    /*
        Getter for User password in this model
        @param None
        @return UserPw
    */
    public String getUserPw() {
        return this.userPw;
    }

    /*
        Getter for Cookie setting in this model
        @param None
        @return EnableCookie
    */
    public boolean getEnableCookie() {
        return this.enableCookie;
    }
}