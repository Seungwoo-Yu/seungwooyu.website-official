// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectModel {
    private Integer projectId;
    @Builder.Default
    private String projectThumbnail = "";
    private String projectTitle;
    private String projectSubtitle;
    private String projectContents;
    @Builder.Default
    private String[] projectFile = null;
    @Builder.Default
    private String projectDirectory = "";
    private String projectAuthor;
    @Builder.Default
    private String projectLanguage = "en";
}