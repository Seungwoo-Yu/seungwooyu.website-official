// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.time.Instant;

@Repository
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LectureModel {
    private Integer lectureId;
    private String lectureTitle;
    private String lectureContents;
    @Builder.Default
    private String lectureDirectory = "";
    private String lectureAuthor;
    @Builder.Default
    private String lectureLanguage = "en";
    private Instant lectureDate;
}