// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "users_rate_table")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRateLimitEntity {
    @Id
    @Column(name = "address")
    private String address;

    @Column(name = "rate_count")
    private Integer rateCount;

    @Column(name = "rate_duration")
    private LocalDateTime rateDuration;

    @Column(name = "ban_duration")
    private LocalDateTime banDuration;

    @Column(name = "past_ban")
    private Integer pastBan;
}