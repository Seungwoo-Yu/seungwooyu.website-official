// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "projects_table")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "projects_uuid")
    private Integer projectId;

    @Column(name = "projects_thumbnail")
    private String projectThumbnail;

    @Column(name = "projects_title")
    private String projectTitle;

    @Column(name = "projects_subtitle")
    private String projectSubtitle;

    @Column(name = "projects_contents")
    private String projectContents;

    @Column(name = "projects_file")
    private String projectFile;

    @Column(name = "projects_directory")
    private String projectDirectory;

    @Column(name = "projects_author")
    private String projectAuthor;

    @Column(name = "projects_language")
    private String projectLanguage;
}