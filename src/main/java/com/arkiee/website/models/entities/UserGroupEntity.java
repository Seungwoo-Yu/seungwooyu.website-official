// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "authorities_table")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserGroupEntity {

    @Id
    @Column(name = "user_id")
    private String userId;

    @Column(name = "user_group")
    private String userGroup;
}