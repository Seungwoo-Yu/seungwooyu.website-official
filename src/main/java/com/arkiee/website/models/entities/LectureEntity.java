// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models.entities;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "lectures_table")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LectureEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lectures_uuid")
    private Integer lectureId;

    @Column(name = "lectures_title")
    private String lectureTitle;

    @Column(name = "lectures_contents")
    private String lectureContents;

    @Column(name = "lectures_directory")
    private String lectureDirectory;

    @Column(name = "lectures_author")
    private String lectureAuthor;

    @Column(name = "lectures_language")
    private String lectureLanguage;

    @Column(name = "lectures_date")
    @Builder.Default
    private Instant lectureDate = Instant.now();
}