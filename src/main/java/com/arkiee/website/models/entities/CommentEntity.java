// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models.entities;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "comments_table")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comments_uuid")
    private Integer commentId;

    @Column(name = "comments_linked_table")
    private String commentLinkedTable;

    @Column(name = "comments_linked_table_id")
    private Integer commentLinkedTableId;

    @Column(name = "comments_contents")
    private String commentContents;

    @Column(name = "comments_author")
    private String commentAuthor;

    @Column(name = "comments_written_date")
    @Builder.Default
    private Instant commentDate = Instant.now();
}