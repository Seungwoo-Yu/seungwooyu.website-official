// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models;

import com.arkiee.website.models.entities.CommentEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentModel {
    private String targetTable;
    private Integer targetTableId;
    private CommentEntity entity;
    private Integer index;
}