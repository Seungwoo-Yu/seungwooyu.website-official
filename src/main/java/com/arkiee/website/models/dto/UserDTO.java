// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models.dto;

// Imports fundamental Java classes
import java.util.HashMap;

public class UserDTO {
    //Elements in this DTO
    private String userId;
    private String userPw;
    private String userPwSalt;
    private String userName;
    private String userEmail;
    private String userCookie;
    private String userCookieDuration;

    /*
        Getter for id in this model
        @param None
        @return Id
    */
    public String getUserId() {
        return this.userId;
    }

    /*
        Setter for id in this model
        @param Id
        @param UserDTO
    */
    public UserDTO setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    /*
        Getter for password in this model
        @param None
        @return Password
    */
    public String getUserPw() {
        return this.userPw;
    }

    /*
        Setter for password in this model
        @param Password
        @param UserDTO
    */
    public UserDTO setUserPw(String userPw) {
        this.userPw = userPw;
        return this;
    }

    /*
        Getter for password in this model
        @param None
        @return Password
    */
    public String getUserPwSalt() {
        return this.userPwSalt;
    }

    /*
        Setter for password in this model
        @param Password
        @param UserDTO
    */
    public UserDTO setUserPwSalt(String userPwSalt) {
        this.userPwSalt = userPwSalt;
        return this;
    }

    /*
        Getter for username in this model
        @param None
        @return Username
    */
    public String getUserName() {
        return this.userName;
    }

    /*
        Setter for username in this model
        @param Username
        @param UserDTO
    */
    public UserDTO setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    /*
        Getter for email in this model
        @param None
        @return Email
    */
    public String getUserEmail() {
        return this.userEmail;
    }

    /*
        Setter for email in this model
        @param Email
        @param UserDTO
    */
    public UserDTO setUserEmail(String userEmail) {
        this.userEmail = userEmail;
        return this;
    }

    /*
        Getter for cookie in this model
        @param None
        @return Session
    */
    public String getUserCookie() {
        return this.userCookie;
    }

    /*
        Setter for cookie in this model
        @param Session
        @param UserDTO
    */
    public UserDTO setUserCookie(String userCookie) {
        this.userCookie = userCookie;
        return this;
    }

    /*
        Getter for session based cookie duration in this model
        @param None
        @return UserSessionDuration
    */
    public String getUserCookieDuration() {
        return this.userCookieDuration;
    }

    /*
        Setter for session based cookie in this model
        @param UserSessionDuration
        @param UserDTO
    */
    public UserDTO setUserCookieDuration(String userSessionDuration) {
        this.userCookieDuration = userSessionDuration;
        return this;
    }

    /*
        toList() for this model
        @param None
        @param (HashMap<Key, Value>) UserDTO
    */
    public HashMap<String, String> toHashMap() {
        HashMap<String, String> UserElementsList = new HashMap<>();
        UserElementsList.put("userId", userId);
        UserElementsList.put("userPw", userPw);
        UserElementsList.put("userPwSalt", userPwSalt);
        UserElementsList.put("userName", userName);
        UserElementsList.put("userEmail", userEmail);
        UserElementsList.put("userCookie", userCookie);
        UserElementsList.put("userCookieDuration", userCookieDuration);
        return UserElementsList;
    }

    /*
        toList() for SQL for this model
        @param None
        @param (HashMap<SQLKey, Value>) UserDTO
    */
    public HashMap<String, String> toHashMapForSQL() {
        HashMap<String, String> UserElementsList = new HashMap<>();
        UserElementsList.put("user_id", userId);
        UserElementsList.put("user_pw", userPw);
        UserElementsList.put("user_pw_salt", userPwSalt);
        UserElementsList.put("user_name", userName);
        UserElementsList.put("user_email", userEmail);
        UserElementsList.put("cookie", userCookie);
        UserElementsList.put("cookie_duration", userCookieDuration);
        return UserElementsList;
    }

    /*
        Overrided toString() for this model
        @param None
        @param (String) UserDTO
    */
    @Override
    public String toString() {
        return "UserModel{" +
                "userId='" + userId + '\'' +
                ", userPw='" + userPw + '\'' +
                ", userPwSalt='" + userPwSalt + '\'' +
                ", userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userCookie='" + userCookie + '\'' +
                ", userCookieDuration='" + userCookieDuration + '\'' +
                '}';
    }
}