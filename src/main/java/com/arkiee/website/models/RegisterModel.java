// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.models;

import org.springframework.stereotype.Repository;

@Repository
public class RegisterModel {
    // Elements in this model
    private String type;
    private String content;
    private boolean isPassed;

    public RegisterModel() {}

    public RegisterModel(String type, String content, boolean isPassed) {
        this.type = type;
        this.content = content;
        this.isPassed = isPassed;
    }

    public String getType() {
        return this.type;
    }

    public String getContent() {
        return this.content;
    }

    public boolean getCondition() {
        return this.isPassed;
    }

    public RegisterModel setCondition(boolean condition) {
        this.isPassed = condition;
        return this;
    }
}