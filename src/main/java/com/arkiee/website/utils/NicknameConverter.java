package com.arkiee.website.utils;

import com.arkiee.website.models.dto.UserDTO;
import com.arkiee.website.repositories.dao.UserDAO;

import java.sql.SQLException;
import java.util.List;

public class NicknameConverter {
    private UserDAO userAccess;

    public NicknameConverter(UserDAO userAccess) {
        this.userAccess = userAccess;
    }

    public String getNickname(String userId) throws SQLException {
        UserDTO userData = new UserDTO().setUserId(userId);
        List<UserDTO> list = userAccess.selectByInput(userData);
        if(list.size() == 1 && list.get(0) != null) {
            return list.get(0).getUserName();
        }
        return userId;
    }
}
