// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.utils;

import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;

import java.nio.ByteBuffer;
import java.util.Random;

public class Sha512 {

    public static String getEncrypt(String source, String salt) {
        final String mixedKey = source + salt;
        SHA3.DigestSHA3 digestSHA3 = new SHA3.Digest512();
        byte[] digest = digestSHA3.digest(mixedKey.getBytes());
        return Hex.toHexString(digest);
    }

    public static String generateSalt() {
        Random saltGenerator = new Random();
        byte[] salt = ByteBuffer.allocate(4).putInt(saltGenerator.nextInt(1000000000)).array();
        return Hex.toHexString(salt);
    }
}