// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.utils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AmazonManager {
    private AmazonS3 amazon;
    private String bucketName;
    private String cacheDirectory;
    private String storageDirectoryPrefix;

    public AmazonManager(AmazonS3 amazon, String bucketName, String storageDirectoryPrefix) {
        this.amazon = amazon;
        this.bucketName = bucketName;
        this.storageDirectoryPrefix = storageDirectoryPrefix;
    }

    public AmazonManager(AmazonS3 amazon, String bucketName, String cacheDirectory, String storageDirectoryPrefix) {
        this.amazon = amazon;
        this.bucketName = bucketName;
        this.cacheDirectory = cacheDirectory;
        this.storageDirectoryPrefix = storageDirectoryPrefix;
    }

    public List<String> allocateToCache(MultipartFile[] files, String directory) throws IOException {
        final String finalCacheDirectory = cacheDirectory + "/" + directory;
        List<String> cachedFileList = new ArrayList<String>();
        for(MultipartFile currentFile : files) {
            if(!currentFile.getOriginalFilename().isEmpty()) {
                final File filePath = new File(finalCacheDirectory + "/" + currentFile.getOriginalFilename());
                if(!filePath.exists()) {
                    if(filePath.getParentFile() != null && !filePath.getParentFile().exists()) {
                        filePath.getParentFile().mkdirs();
                    }
                    filePath.createNewFile();
                    BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(filePath));
                    outputStream.write(currentFile.getBytes());
                    outputStream.flush();
                    outputStream.close();
                    cachedFileList.add(filePath.getPath());
                }
            }
        }
        return cachedFileList;
    }

    public void deallocateFromCache(String directory) {
        final String finalCacheDirectory = cacheDirectory + "/" + directory;
        final File directoryPath = new File(finalCacheDirectory);
        for(File file : directoryPath.listFiles()) {
            file.delete();
        }
        directoryPath.delete();
    }

    public List<String> uploadToAmazon(String directory) throws AmazonClientException {
        final String finalCacheDirectory = cacheDirectory + "/" + directory;
        final String finalStorageDirectory = storageDirectoryPrefix + "/" + directory;
        final File directoryPath = new File(finalCacheDirectory);
        List<String> urlList = new ArrayList<String>();
        for(File file : directoryPath.listFiles()) {
            amazon.putObject(new PutObjectRequest(bucketName, finalStorageDirectory + "/" + file.getName(), file)
                            .withCannedAcl(CannedAccessControlList.PublicRead));
            urlList.add(amazon.getUrl(bucketName, finalStorageDirectory + "/" + file.getName()).toString());
        }
        return urlList;
    }

    public void removeFromAmazon(String directory) throws AmazonClientException {
        final String finalStorageDirectory = storageDirectoryPrefix + "/" + directory;
        ObjectListing objectList = amazon.listObjects(new ListObjectsRequest().withBucketName(bucketName)
                                                                                .withPrefix(finalStorageDirectory));
        while (true) {
            for (S3ObjectSummary objectSummary : objectList.getObjectSummaries()) {
                amazon.deleteObject(new DeleteObjectRequest(bucketName, objectSummary.getKey()));
            }
            if (objectList.isTruncated()) {
                objectList = amazon.listNextBatchOfObjects(objectList);
            } else {
                break;
            }
        }
    }
}