// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved
package com.arkiee.website;

// Imports fundamental classes provided by Spring
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

// Sets up for Spring
@SpringBootApplication
@EnableRedisHttpSession
public class WebsiteApplication extends SpringBootServletInitializer {

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebsiteApplication.class);
	}
	public static void main(String[] args) {
		SpringApplication.run(WebsiteApplication.class, args);
	}

}
