// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.interceptors;

import com.arkiee.website.models.entities.UserRateLimitEntity;
import com.arkiee.website.repositories.jpa.UserRateLimitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class RateLimitInterceptor extends HandlerInterceptorAdapter {
    
    private final UserRateLimitRepository repository;

    @Value("${server.rate-limit.rate-interval}")
    private Integer rateInterval;

    @Value("${server.rate-limit.max-rate}")
    private Integer maxRate;

    @Value("${server.rate-limit.past-ban-interval}")
    private Integer pastBanInterval;

    @Value("${server.rate-limit.ban-interval}")
    private Integer banInterval;

    public RateLimitInterceptor(UserRateLimitRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        LocalDateTime time = LocalDateTime.now();
        PastBanInvalidator(time);
        UserRateLimitEntity userRateLimit = repository.findByAddress(request.getRemoteAddr());
        if(userRateLimit == null) {
            userRateLimit = UserRateLimitEntity.builder().address(request.getRemoteAddr())
                                                    .rateCount(0)
                                                    .rateDuration(time.plusSeconds(rateInterval))
                                                    .pastBan(0)
                                                    .banDuration(time)
                                                    .build();
        }
        rateCounter(userRateLimit, time);
        if(userRateLimit.getRateCount() >= maxRate || userRateLimit.getBanDuration().isAfter(LocalDateTime.now())) {
            response.sendError(HttpStatus.TOO_MANY_REQUESTS.value());
        }
        return super.preHandle(request, response, handler);
    }

    private void PastBanInvalidator(LocalDateTime time) {
        List<UserRateLimitEntity> list = repository.findAll();
        for (UserRateLimitEntity user : list) {
            if(user.getBanDuration().isBefore(time)) {
                if(user.getBanDuration().isBefore(time.minusHours(pastBanInterval))) {
                    user.setPastBan(0);
                } else if(user.getPastBan() >= 1) {
                    user.setPastBan(user.getPastBan() - 1);
                }
                user.setBanDuration(time);
            }
        }
        repository.saveAll(list);
    }

    private void rateCounter(UserRateLimitEntity userRateLimit, LocalDateTime time) {
        if(userRateLimit.getRateCount() < maxRate) {
            if(userRateLimit.getRateDuration().isAfter(time)) {
                userRateLimit.setRateCount(userRateLimit.getRateCount() + 1);
                if(userRateLimit.getRateCount() >= maxRate) {
                    userRateLimit.setBanDuration(time.plusMinutes(banInterval + (banInterval * userRateLimit.getPastBan())));
                    userRateLimit.setPastBan(userRateLimit.getPastBan() + 1);
                }
            } else {
                userRateLimit.setRateCount(1);
                userRateLimit.setRateDuration(time.plusSeconds(rateInterval));
            }
            repository.save(userRateLimit);
        }
    }
}