// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.repositories.dao;

// Imports fundamental Spring classes.

import com.arkiee.website.models.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

// Imports fundamental Java classes.
// Imports DTO to save into.

// Sets up repository for DAO.
@Repository
public class UserDAO {
    // Autowire Jdbc to connect Database
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /*
        All user information Selector to Database
        @param None
        @param List<UserDTO> from DB
    */
    public List<UserDTO> selectAllRows() throws SQLException, DataAccessException {
        return jdbcTemplate.query("select * from users_table", (ResultSet request, int rowNum) -> {
            UserDTO dataTransfer = new UserDTO();
            dataTransfer.setUserId(request.getString("user_id"));
            dataTransfer.setUserName(request.getString("user_name"));
            dataTransfer.setUserEmail(request.getString("user_email"));
            return dataTransfer;
        });
    }

    /*
        User information Selector to Database by Credential
        @param UserDTO
        @param UserDTO from DB
    */
    public UserDTO selectByCredential(UserDTO data) throws SQLException, DataAccessException {
        return jdbcTemplate.queryForObject("select * from users_table where user_id=? and user_pw=?",
            new Object[]{
                data.getUserId(), data.getUserPw()
            }, (ResultSet request, int rownum) -> getUserDTO(request)
        );
    }

    /*
        All user infomation Selecter to Database by Input
        @param UserDTO
        @param List<UserDTO> from DB
    */
    public List<UserDTO> selectByInput(UserDTO data) throws SQLException, DataAccessException {
        StringBuilder executeCommand = new StringBuilder("select * from users_table where ");
        HashMap<String, String> map = data.toHashMapForSQL();
        for(String key : map.keySet()) {
            if(map.get(key) != null && map.get(key) != ""){
                executeCommand.append(key).append(" = LOWER('").append(map.get(key)).append("') and ");
            }
        }
        executeCommand.delete(executeCommand.length() - 4, executeCommand.length());

        return jdbcTemplate.query(executeCommand.toString(), (request, rowNum) -> getUserDTO(request));
    }

    private UserDTO getUserDTO(ResultSet request) throws SQLException {
        UserDTO dataTransfer = new UserDTO();
        dataTransfer.setUserId(request.getString("user_id"));
        dataTransfer.setUserName(request.getString("user_name"));
        dataTransfer.setUserEmail(request.getString("user_email"));
        dataTransfer.setUserCookie(request.getString("cookie"));
        dataTransfer.setUserCookieDuration(request.getString("cookie_duration"));
        return dataTransfer;
    }

    /*
        Salt selecter to Database
        @param UserDTO
        @param UserDTO from DB
    */
    public UserDTO selectSalt(UserDTO data) throws DataAccessException {
        return jdbcTemplate.queryForObject("select * from users_table where user_id=?",
            new Object[]{
                data.getUserId()
            }, (ResultSet request, int rownum) -> {
                UserDTO dataTransfer = new UserDTO();
                dataTransfer.setUserPwSalt(request.getString("user_pw_salt"));
                return dataTransfer;
            }
        );
    }

    /*
        Inserter to Database
        @param UserDTO
        @param None
    */
    public void insert(UserDTO dataTransfer) throws DataAccessException {
        jdbcTemplate.update("insert into users_table(user_id, user_pw, user_pw_salt, user_name, user_email) values(?, ?, ?, ?, ?)",
            dataTransfer.getUserId(), dataTransfer.getUserPw(), dataTransfer.getUserPwSalt(),
            dataTransfer.getUserName(), dataTransfer.getUserEmail()
        );
    }

    /*
        Updater to Database
        @param UserDTO
        @param None
    */
    public void update(UserDTO dataTransfer) throws DataAccessException {
        jdbcTemplate.update("update set user_pw=?, user_name=?, user_email=? where user_id = ?",
        dataTransfer.getUserPw(), dataTransfer.getUserName(), dataTransfer.getUserEmail(), dataTransfer.getUserId()
        );
    }

    public void updateByInput(UserDTO data) throws DataAccessException {
        StringBuilder executeCommand = new StringBuilder("update users_table set ");
        HashMap<String, String> map = data.toHashMapForSQL();
        for(String key : map.keySet()) {
            if(map.get(key) != null && map.get(key) != "" && key != "user_id"){
                executeCommand.append(key).append(" = '").append(map.get(key)).append("', ");
            }
        }
        executeCommand.delete(executeCommand.length() - 2, executeCommand.length())
            .append(" where user_id = '").append(data.getUserId()).append("'");

        jdbcTemplate.update(executeCommand.toString());
    }

    /*
        Deleter to Database
        @param UserDTO
        @param None
    */
    public void delete(UserDTO dataTransfer) throws DataAccessException {
        jdbcTemplate.update("delete from users_table where user_id = ?", dataTransfer.getUserId());
        dataTransfer.setUserId("").setUserPw("").setUserPwSalt("").setUserName("")
                    .setUserEmail("").setUserCookie("").setUserCookieDuration("");
    }
}