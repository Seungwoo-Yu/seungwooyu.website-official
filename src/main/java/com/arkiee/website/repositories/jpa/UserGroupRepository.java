// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.repositories.jpa;

import com.arkiee.website.models.entities.UserGroupEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGroupRepository extends CrudRepository<UserGroupEntity, String> {
    UserGroupEntity findByUserId(String userId);
}