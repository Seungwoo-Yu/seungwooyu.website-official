// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.repositories.jpa;

import com.arkiee.website.models.entities.ProjectEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends CrudRepository<ProjectEntity, String> {
    List<ProjectEntity> findAll();
    @Query(nativeQuery = true, value = "select * from projects_table where projects_language = :projectLanguage limit 10 offset :projectOffset")
    List<ProjectEntity> findAllByProjectLanguage(@Param("projectLanguage") String projectLanguage, @Param("projectOffset") Integer projectOffset);
    @Query(nativeQuery = true, value = "select * from projects_table where (projects_title like %:searchValue% or projects_subtitle like %:searchValue% or projects_contents like %:searchValue% or projects_author like %:searchValue%) and projects_language = :projectLanguage")
    List<ProjectEntity> findBySearchValueAndProjectLanguage(@Param("searchValue") String searchValue, @Param("projectLanguage") String projectLanguage);
    ProjectEntity findByProjectId(Integer projectId);
}