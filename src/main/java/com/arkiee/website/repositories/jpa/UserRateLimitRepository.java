// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.repositories.jpa;

import com.arkiee.website.models.entities.UserRateLimitEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRateLimitRepository extends CrudRepository<UserRateLimitEntity, String> {
    List<UserRateLimitEntity> findAll();
    UserRateLimitEntity findByAddress(String address);
}