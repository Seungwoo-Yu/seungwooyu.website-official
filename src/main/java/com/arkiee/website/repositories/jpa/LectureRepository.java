// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.repositories.jpa;

import com.arkiee.website.models.entities.LectureEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LectureRepository extends CrudRepository<LectureEntity, String> {
    List<LectureEntity> findAll();
    @Query(nativeQuery = true, value = "select * from lectures_table where lectures_language = :lectureLanguage limit 5 offset :lectureOffset")
    List<LectureEntity> findAllByLectureLanguage(@Param("lectureLanguage") String lectureLanguage, @Param("lectureOffset") Integer lectureOffset);
    @Query(nativeQuery = true, value = "select * from lectures_table where (lectures_title like %:searchValue% or lectures_contents like %:searchValue% or lectures_author like %:searchValue%) and lectures_language = :lectureLanguage")
    List<LectureEntity> findBySearchValueAndLectureLanguage(@Param("searchValue") String searchValue, @Param("lectureLanguage") String projectLanguage);
    LectureEntity findByLectureId(Integer lectureId);
}