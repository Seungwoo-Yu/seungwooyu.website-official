// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.repositories.jpa;

import com.arkiee.website.models.entities.CommentEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<CommentEntity, String> {
    List<CommentEntity> findAll();
    @Query(nativeQuery = true, value = "select * from comments_table where comments_linked_table = :table and comments_linked_table_id = :table_id limit 15 offset :commentOffset")
    List<CommentEntity> findAllByTableNameAndTableId(@Param("table") String commentLinkedTable, @Param("table_id") Integer commentLinkedTableId, @Param("commentOffset") Integer commentOffset);
    CommentEntity findByCommentId(Integer commentId);
}