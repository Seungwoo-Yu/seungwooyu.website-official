// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.services;

import com.arkiee.website.models.dto.UserDTO;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.utils.Sha512;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;

// Sets up for service.
@Service
public class ProfileService {
    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession session;
    UserDTO userTransfer;
    UserDAO userAccess;

    public ProfileService() {
    }

    public ProfileService(UserDAO userAccess) {
        this.userAccess = userAccess;
    }

    public ProfileService(HttpServletRequest request, HttpServletResponse response, UserDTO userTransfer, UserDAO userAccess) {
        this.request = request;
        this.response = response;
        this.userTransfer = userTransfer;
        this.userAccess = userAccess;
    }

    public ProfileService(HttpServletRequest request, HttpServletResponse response, HttpSession session, UserDAO userAccess) {
        this.request = request;
        this.response = response;
        this.session = session;
        this.userAccess = userAccess;
    }

    // Sets values up to use
    public final String init() {
        return "/html/profile";
    }

    public final String getProfile() {
        try {
            UserDTO userTransfer = new UserDTO();
            if(session.getAttribute("Id") != null) {
                userTransfer.setUserId(session.getAttribute("Id").toString());
            }
            List<UserDTO> result = userAccess.selectByInput(userTransfer);
            if (result.size() == 1 && result.get(0) != null) {
                ObjectMapper mapper = new ObjectMapper();
                UserDTO modifiedResult = result.get(0).setUserPw(null).setUserPwSalt(null)
                                                        .setUserCookie(null).setUserCookieDuration(null);
                return mapper.writeValueAsString(modifiedResult);
            }
        } catch (JsonProcessingException | SQLException | DataAccessException ignored) {
        }
        return "/";
    }

    public final String updateProfile() {
        try {
            if(userTransfer.getUserId() != null) {
                if(userTransfer.getUserPw() != null) {
                    String salt = Sha512.generateSalt();
                    String newEncrypted = Sha512.getEncrypt(userTransfer.getUserPw(), salt);
                    userTransfer.setUserPw(newEncrypted);
                    userAccess.updateByInput(userTransfer);
                }
                userAccess.updateByInput(userTransfer);
                return "/profile/success";
            }
        } catch (DataAccessException ignored) {
        }
        return "/profile/failure";
    }
}