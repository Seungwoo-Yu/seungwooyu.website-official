// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.services;

import com.amazonaws.services.s3.AmazonS3;
import com.arkiee.website.models.FileModel;
import com.arkiee.website.models.ProjectModel;
import com.arkiee.website.models.entities.ProjectEntity;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.repositories.jpa.ProjectRepository;
import com.arkiee.website.utils.AmazonManager;
import com.arkiee.website.utils.NicknameConverter;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.List;

// Sets up for service.
@Service
public class ProjectService {
    private HttpServletRequest request;
    private ProjectRepository repository;
    private UserDAO userRepository;
    private ProjectModel projectModel;
    private ObjectMapper objectMapper = new ObjectMapper();
    private FileModel fileModel;
    private HttpSession session;
    private AmazonS3 amazon;
    private String bucketName;
    private String cacheDirectory;
    private String storageDirectoryPrefix;

    public ProjectService() {
    }

    public ProjectService(AmazonS3 amazon, String bucketName, String cacheDirectory, String storageDirectoryPrefix,
            FileModel fileModel) {
        this.amazon = amazon;
        this.bucketName = bucketName;
        this.cacheDirectory = cacheDirectory;
        this.storageDirectoryPrefix = storageDirectoryPrefix;
        this.fileModel = fileModel;
    }

    public ProjectService(AmazonS3 amazon, String bucketName, String storageDirectoryPrefix, HttpSession session,
            ProjectRepository repository) {
        this.amazon = amazon;
        this.bucketName = bucketName;
        this.storageDirectoryPrefix = storageDirectoryPrefix;
        this.session = session;
        this.repository = repository;
    }

    public ProjectService(HttpServletRequest request, ProjectRepository repository, UserDAO userRepository) {
        this.request = request;
        this.repository = repository;
        this.userRepository = userRepository;
    }

    public ProjectService(HttpSession session, ProjectRepository repository, ProjectModel projectModel) {
        this.session = session;
        this.repository = repository;
        this.projectModel = projectModel;
    }

    // Sets values up to use
    public final String init() {
        return "/html/project/project";
    }

    public final String addPost() {
        return "/html/project/project_post";
    }

    public final String viewPost() {
        return "/html/project/project_view";
    }

    public final String editPost() {
        return "/html/project/project_edit";
    }

    public final String searchProject() {
        try {
            if(request.getHeader("searchValue") != null) {
                String searchValue = URLDecoder.decode(request.getHeader("searchValue"), StandardCharsets.UTF_8.toString());
                List<ProjectEntity> list = repository.findBySearchValueAndProjectLanguage(searchValue, request.getHeader("pageLanguage"));
                for (ProjectEntity entity : list) {
                    NicknameConverter converter = new NicknameConverter(userRepository);
                    entity.setProjectAuthor(converter.getNickname(entity.getProjectAuthor()));
                }
                if(list.size() > 0) {
                    return objectMapper.writeValueAsString(list);
                }
            }
        } catch (JsonProcessingException | UnsupportedEncodingException | SQLException ignored) {
        }
        return "";
    }

    public final String getProjectList() {
        try {
            if (request.getHeader("PageLanguage") != null && request.getHeader("pageIndex") != null) {
                List<ProjectEntity> list = repository.findAllByProjectLanguage(request.getHeader("pageLanguage"),
                        Integer.parseInt(request.getHeader("pageIndex")));
                for (ProjectEntity entity : list) {
                    NicknameConverter converter = new NicknameConverter(userRepository);
                    entity.setProjectAuthor(converter.getNickname(entity.getProjectAuthor()));
                }
                return objectMapper.writeValueAsString(list);
            }
        } catch (JsonProcessingException | SQLException ignored) {
        }
        return "/error/projects";
    }

    public final String getProjectContents() {
        try {
            if (request.getHeader("ProjectId") != null) {
                ProjectEntity projectEntity = repository
                        .findByProjectId(Integer.parseInt(request.getHeader("projectId")));
                NicknameConverter converter = new NicknameConverter(userRepository);
                projectEntity.setProjectAuthor(converter.getNickname(projectEntity.getProjectAuthor()));
                return objectMapper.writeValueAsString(projectEntity);
            }
        } catch (JsonProcessingException | SQLException ignored) {
        }
        return "/error/projects";
    }

    public final String fileUpload() {
        if (fileModel.getFile() != null) {
            try {
                final AmazonManager amazonManager = new AmazonManager(amazon, bucketName, cacheDirectory,
                        storageDirectoryPrefix);
                final List<String> allocatedResults = amazonManager.allocateToCache(fileModel.getFile(),
                        fileModel.getDirectory());
                final List<String> allocatedUrlResults = amazonManager.uploadToAmazon(fileModel.getDirectory());
                if (allocatedResults.size() > 0 && allocatedUrlResults.size() > 0
                        && allocatedResults.size() == allocatedUrlResults.size()) {
                    String result = objectMapper.writeValueAsString(allocatedUrlResults);
                    amazonManager.deallocateFromCache(fileModel.getDirectory());
                    return result;
                }
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }
        }
        return "failure";
    }

    public final String postProject() {
        try {
            String files = objectMapper.writeValueAsString(projectModel.getProjectFile());
            ProjectEntity projectEntity = ProjectEntity.builder().projectContents(projectModel.getProjectContents())
                                                            .projectDirectory(projectModel.getProjectDirectory())
                                                            .projectFile(files)
                                                            .projectLanguage(projectModel.getProjectLanguage())
                                                            .projectSubtitle(projectModel.getProjectSubtitle())
                                                            .projectThumbnail(projectModel.getProjectThumbnail())
                                                            .projectTitle(projectModel.getProjectTitle())
                                                            .build();
            projectEntity.setProjectAuthor(session.getAttribute("id").toString());
            repository.save(projectEntity);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public final String updateProject() {
        try {
            final ProjectEntity existedEntity = repository.findByProjectId(projectModel.getProjectId());
            if (existedEntity.getProjectAuthor().equals(session.getAttribute("id").toString())) {
                String files = objectMapper.writeValueAsString(projectModel.getProjectFile());
                ProjectEntity projectEntity = ProjectEntity.builder().projectContents(projectModel.getProjectContents())
                                                                .projectDirectory(projectModel.getProjectDirectory())
                                                                .projectFile(files)
                                                                .projectLanguage(projectModel.getProjectLanguage())
                                                                .projectSubtitle(projectModel.getProjectSubtitle())
                                                                .projectThumbnail(projectModel.getProjectThumbnail())
                                                                .projectTitle(projectModel.getProjectTitle())
                                                                .projectId(projectModel.getProjectId())
                                                                .build();
                projectEntity.setProjectAuthor(session.getAttribute("id").toString());
                repository.save(projectEntity);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public final String deleteProject(Integer index) {
        if (index > 0) {
            final AmazonManager amazonManager = new AmazonManager(amazon, bucketName, storageDirectoryPrefix);
            final ProjectEntity projectEntity = repository.findByProjectId(index);
            if (projectEntity != null) {
                if (projectEntity.getProjectAuthor().equals(session.getAttribute("Id").toString())) {
                    amazonManager.removeFromAmazon(projectEntity.getProjectDirectory());
                    repository.delete(projectEntity);
                }
            }
        }
        return "";
    }

    public final String deleteProjects(String indexes) {
        Integer[] extracted;
        try {
            extracted = objectMapper.readValue(indexes, Integer[].class);
            for (Integer project : extracted) {
                deleteProject(project);
            }
        } catch (IOException ignored) {
        }
        return "";
    }
}