// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.services;

import com.amazonaws.services.s3.AmazonS3;
import com.arkiee.website.models.FileModel;
import com.arkiee.website.models.LectureModel;
import com.arkiee.website.models.entities.LectureEntity;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.repositories.jpa.LectureRepository;
import com.arkiee.website.utils.AmazonManager;
import com.arkiee.website.utils.NicknameConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.List;

// Sets up for service.
@Service
public class LectureService {
    private HttpServletRequest request;
    private LectureRepository repository;
    private UserDAO userRepository;
    private HttpSession session;
    private LectureModel lectureModel;
    private ObjectMapper objectMapper = new ObjectMapper();
    private AmazonS3 amazon;
    private String bucketName;
    private String cacheDirectory;
    private String storageDirectoryPrefix;
    private FileModel fileModel;

    public LectureService() { }

    public LectureService(AmazonS3 amazon, String bucketName, String cacheDirectory, String storageDirectoryPrefix, FileModel fileModel) {
        this.amazon = amazon;
        this.bucketName = bucketName;
        this.cacheDirectory = cacheDirectory;
        this.storageDirectoryPrefix = storageDirectoryPrefix;
        this.fileModel = fileModel;
    }

    public LectureService(AmazonS3 amazon, String bucketName, String storageDirectoryPrefix, HttpSession session, LectureRepository repository) {
        this.amazon = amazon;
        this.bucketName = bucketName;
        this.storageDirectoryPrefix = storageDirectoryPrefix;
        this.session = session;
        this.repository = repository;
    }

    public LectureService(HttpServletRequest request, LectureRepository repository, UserDAO userRepository) {
        this.request = request;
        this.repository = repository;
        this.userRepository = userRepository;
    }

    public LectureService(HttpSession session, LectureRepository repository, LectureModel lectureModel) {
        this.session = session;
        this.repository = repository;
        this.lectureModel = lectureModel;
    }

    // Sets values up to use
    public final String init() {
        return "/html/lecture";
    }

    public final String searchLecture() {
        try {
            if(request.getHeader("searchValue") != null) {
                String searchValue = URLDecoder.decode(request.getHeader("searchValue"), StandardCharsets.UTF_8.toString());
                List<LectureEntity> list = repository.findBySearchValueAndLectureLanguage(searchValue, request.getHeader("pageLanguage"));
                for (LectureEntity entity : list) {
                    NicknameConverter converter = new NicknameConverter(userRepository);
                    entity.setLectureAuthor(converter.getNickname(entity.getLectureAuthor()));
                }
                if(list.size() > 0) {
                    return objectMapper.writeValueAsString(list);
                }
            }
        } catch (JsonProcessingException | UnsupportedEncodingException | SQLException ignored) {
        }
        return "";
    }

    public final String getLectureList() {
        try {
            if (request.getHeader("PageLanguage") != null && request.getHeader("PageIndex") != null) {
                List<LectureEntity> list = repository.findAllByLectureLanguage(request.getHeader("PageLanguage"),
                        Integer.parseInt(request.getHeader("PageIndex")));
                for (LectureEntity entity : list) {
                    NicknameConverter converter = new NicknameConverter(userRepository);
                        entity.setLectureAuthor(converter.getNickname(entity.getLectureAuthor()));
                }
                return objectMapper.writeValueAsString(list);
            }
        } catch (JsonProcessingException | SQLException ignored) {
        }
        return "/error/lectures";
    }

    public final String fileUpload() {
        if (fileModel.getFile() != null) {
            try {
                final AmazonManager amazonManager = new AmazonManager(amazon, bucketName, cacheDirectory,
                        storageDirectoryPrefix);
                final List<String> allocatedResults = amazonManager.allocateToCache(fileModel.getFile(),
                        fileModel.getDirectory());
                final List<String> allocatedUrlResults = amazonManager.uploadToAmazon(fileModel.getDirectory());
                if (allocatedResults.size() > 0 && allocatedUrlResults.size() > 0
                        && allocatedResults.size() == allocatedUrlResults.size()) {
                    String result = objectMapper.writeValueAsString(allocatedUrlResults);
                    amazonManager.deallocateFromCache(fileModel.getDirectory());
                    return result;
                }
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }
        }
        return "failure";
    }

    public final String postLecture() {
        LectureEntity lectureEntity = LectureEntity.builder().lectureAuthor(lectureModel.getLectureAuthor())
                                                        .lectureContents(lectureModel.getLectureContents())
                                                        .lectureDirectory(lectureModel.getLectureDirectory())
                                                        .lectureLanguage(lectureModel.getLectureLanguage())
                                                        .lectureTitle(lectureModel.getLectureTitle())
                                                        .build();
        lectureEntity.setLectureAuthor(session.getAttribute("Id").toString());
        repository.save(lectureEntity);
        return "";
    }

    public final String updateLecture() {
        final LectureEntity existedEntity = repository.findByLectureId(lectureModel.getLectureId());
        if (existedEntity.getLectureAuthor().equals(session.getAttribute("Id").toString())) {
            LectureEntity lectureEntity = LectureEntity.builder().lectureContents(lectureModel.getLectureContents())
                                                            .lectureDirectory(lectureModel.getLectureDirectory())
                                                            .lectureLanguage(lectureModel.getLectureLanguage())
                                                            .lectureTitle(lectureModel.getLectureTitle())
                                                            .lectureId(lectureModel.getLectureId())
                                                            .build();
            lectureEntity.setLectureAuthor(session.getAttribute("Id").toString());
            repository.save(lectureEntity);
        }
        return "";
    }

    public final String deleteLecture(Integer index) {
        if (index > 0) {
            final AmazonManager amazonManager = new AmazonManager(amazon, bucketName, storageDirectoryPrefix);
            final LectureEntity lectureEntity = repository.findByLectureId(index);
            if (lectureEntity != null) {
                if (lectureEntity.getLectureAuthor().equals(session.getAttribute("Id").toString())) {
                    amazonManager.removeFromAmazon(lectureEntity.getLectureDirectory());
                    repository.delete(lectureEntity);
                }
            }
        }
        return "";
    }
}