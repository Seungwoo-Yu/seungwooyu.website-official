// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.services;

import com.arkiee.website.models.CommentModel;
import com.arkiee.website.models.dto.UserDTO;
import com.arkiee.website.models.entities.CommentEntity;
import com.arkiee.website.models.entities.UserGroupEntity;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.repositories.jpa.CommentRepository;
import com.arkiee.website.repositories.jpa.UserGroupRepository;
import com.arkiee.website.utils.NicknameConverter;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

// Sets up for service.
@Service
public class CommentService {
    private HttpSession session;
    private CommentRepository repository;
    private UserDAO userRepository;
    private UserGroupRepository userGroup;
    private CommentEntity commentEntity;
    private ObjectMapper objectMapper = new ObjectMapper();
    private String targetTable;
    private Integer targetTableId;
    private Integer index;
    private Integer id;

    public CommentService() {
    }

    public CommentService(HttpServletRequest request, CommentRepository repository, UserDAO userRepository) {
        try {
            CommentModel commentModel = objectMapper.readValue(request.getHeader("Comment").toString(), CommentModel.class);
            this.targetTable = commentModel.getTargetTable();
            this.targetTableId = commentModel.getTargetTableId();
            this.userRepository = userRepository;
            this.index = commentModel.getIndex();
        } catch (IOException ignored) {
        }
        this.repository = repository;
    }

    public CommentService(HttpSession session, CommentRepository repository, CommentModel commentModel) {
        this.session = session;
        this.repository = repository;
        this.commentEntity = commentModel.getEntity();
        this.id = commentModel.getEntity().getCommentId();
    }

    public CommentService(HttpSession session, CommentRepository repository, CommentModel commentModel, UserGroupRepository userGroup) {
        this.session = session;
        this.repository = repository;
        this.userGroup = userGroup;
        this.commentEntity = commentModel.getEntity();
        this.id = commentModel.getEntity().getCommentId();
    }

    public String getComments() {
        try {
            List<CommentEntity> list = repository.findAllByTableNameAndTableId(targetTable, targetTableId, index);
            for(CommentEntity entity : list) {
                NicknameConverter converter = new NicknameConverter(userRepository);
                entity.setCommentAuthor(converter.getNickname(entity.getCommentAuthor()));
            }
            return objectMapper.writeValueAsString(list);
        } catch (JsonProcessingException | SQLException e) {
            e.printStackTrace();
        }
        return "/";
    }

    public String addComment() {
        commentEntity.setCommentAuthor(session.getAttribute("Id").toString());
        repository.save(commentEntity);
        return "/";
    }

    public String editComment() {
        CommentEntity oldCommentEntity = repository.findByCommentId(id);
        if(oldCommentEntity != null) {
            if(oldCommentEntity.getCommentAuthor().equals(session.getAttribute("Id").toString())) {
                oldCommentEntity.setCommentContents(commentEntity.getCommentContents());
                repository.save(oldCommentEntity);
            }
        }
        return "/";
    }

    public String deleteComment() {
        CommentEntity oldCommentEntity = repository.findByCommentId(id);
        UserGroupEntity group = userGroup.findByUserId(session.getAttribute("Id").toString());
        if(oldCommentEntity != null) {
            if(oldCommentEntity.getCommentAuthor().equals(session.getAttribute("Id").toString()) || group.getUserGroup().equals("ADMIN")) {
                repository.delete(oldCommentEntity);
            }
        }
        return "/";
    }
}