// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.services;

import com.arkiee.website.models.LoginModel;
import com.arkiee.website.models.dto.UserDTO;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.utils.Sha512;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

// Sets up for service.
@Service
public class LoginService {
    private LoginModel model = new LoginModel();
    private UserDAO dataAccess;
    private UserDTO credential = new UserDTO();
    private HttpServletResponse response;
    private HttpServletRequest request;
    private HttpSession session;
    private Cookie cookie;
    private ObjectMapper objectMapper = new ObjectMapper();

    public LoginService() {
    }

    public LoginService(HttpSession session, HttpServletRequest request) {
        this.session = session;
        this.request = request;
        cookie = WebUtils.getCookie(this.request, "login");
    }

    public LoginService(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            UserDAO dataAccess) {
        this.dataAccess = dataAccess;
        this.session = session;
        this.request = request;
        this.response = response;
        cookie = WebUtils.getCookie(this.request, "login");
    }

    public final String init() {
        if (cookie != null && cookie.getValue() != "" && cookie.getValue() != null) {
            final String key = cookie.getValue();
            credential.setUserCookie(key);
            try {
                List<UserDTO> result = dataAccess.selectByInput(credential);
                if (result.size() == 1 && result.get(0).getUserId() != "" && result.get(0).getUserId() == credential.getUserId()) {
                    String sessionKey = encrypt(key, Sha512.generateSalt());
                    // Process of Session
                    setSession(sessionKey, result.get(0).getUserId());
                    // Process of Cookie
                    setCookie(result.get(0), sessionKey, generateTime().toString());
                } else {
                    cookie.setValue("");
                    cookie.setMaxAge(0);
                }
            } catch (DataAccessException e) {
                return "/login/failure";
            } catch (SQLException e) {
                return "/login/failure";
            }
        }
        if ((cookie != null && cookie.getValue() != "" && cookie.getValue() != null
                && session.getAttribute("Credential") != null) || session.getAttribute("Credential") != null) {
            return "redirect:/";
        }
        return "/html/login";
    }

    public final String login() {
        try {
            model = objectMapper.readValue(request.getHeader("loginProcess"), LoginModel.class);
            //Prepare Password and its salt for Session and Cookie
            authorize();
            UserDTO result = dataAccess.selectByCredential(credential);
            if(result.getUserId() != "" && result.getUserId() != credential.getUserId()) {
                String sessionKey = encrypt(result.getUserPw(), Sha512.generateSalt());
                //Process of Session
                setSession(sessionKey, result.getUserId());
                //Process of Cookie
                if(model.getEnableCookie()) {
                    createCookie(sessionKey);
                    //Update cookie and its duration to DB
                    setCookie(result, sessionKey, generateTime().toString());
                }
                return "/";
            }
        } catch (DataAccessException e) {
        } catch (SQLException e) {
        } catch (JsonParseException e) {
        } catch (JsonMappingException e) {
        } catch (IOException e) {
        }
        return "/login/failure";
    }

    public final String logout() {
        if(cookie != null){
            if(cookie.getValue() != null) {
                cookie.setValue("");
                cookie.setMaxAge(0);
            }
        }
        
        session.setAttribute("Credential", null);
        session.setAttribute("Id", null);
        session.removeAttribute("Credential");
        session.removeAttribute("Id");
        session.invalidate();
        return "/";
    }

    private final void authorize() throws DataAccessException, SQLException {
        if(credential.getUserId() == null && credential.getUserPw() == null) {
            credential.setUserId(model.getUserId()).setUserPw(model.getUserPw());
        }
        final String salt = dataAccess.selectSalt(credential).getUserPwSalt();
        final String encrypted = Sha512.getEncrypt(credential.getUserPw(), salt);
        credential.setUserPw(encrypted).setUserPwSalt(salt);
    }

    private final void createCookie(String value) {
        Cookie credentialData = new Cookie("login", value);
        credentialData.setPath("/");
        credentialData.setHttpOnly(true);
        credentialData.setMaxAge(60 * 60 * 24 * 7);
        response.addCookie(credentialData);
    }

    private final Date generateTime() {
        LocalDateTime currentTime = LocalDateTime.now().plusDays(7);
        return Date.valueOf(currentTime.toLocalDate());
    }

    private final String encrypt(String password, String salt) {
        return Sha512.getEncrypt(password, salt).substring(0, 30);
    }

    private final void setSession(String credential, String id) {
        session.setAttribute("Credential", credential);
        session.setAttribute("Id", id);
    }

    private final void setCookie(UserDTO userTransfer, String value, String duration) throws DataAccessException, SQLException {
        createCookie(value);
        //Update cookie and its duration to DB
        userTransfer.setUserCookie(value).setUserCookieDuration(duration);
        dataAccess.updateByInput(userTransfer);
    }
}