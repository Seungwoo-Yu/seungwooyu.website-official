// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.services;

import com.arkiee.website.models.RegisterModel;
import com.arkiee.website.models.dto.UserDTO;
import com.arkiee.website.models.entities.UserGroupEntity;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.repositories.jpa.UserGroupRepository;
import com.arkiee.website.utils.Sha512;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

// Sets up for service.
@Service
public class RegisterService {
    private UserDTO dataTransfer = new UserDTO();
    private UserDAO dataAccess;
    private RegisterModel model = new RegisterModel();
    private HttpSession session;
    private UserGroupRepository userGroupRepository;
    private HttpServletRequest request;
    private ObjectMapper objectMapper = new ObjectMapper();

    public RegisterService() {
    }

    public RegisterService(UserDAO dataAccess) {
        this.dataAccess = dataAccess;
    }

    public RegisterService(HttpSession session, UserDAO dataAccess) {
        this.dataAccess = dataAccess;
        this.session = session;
    }

    public RegisterService(HttpServletRequest request, UserDAO dataAccess) {
        this.request = request;
        this.dataAccess = dataAccess;
    }

    public RegisterService(HttpServletRequest request, UserDAO dataAccess, UserDTO dataTransfer, UserGroupRepository userGroupRepository) {
        this.request = request;
        this.dataAccess = dataAccess;
        this.dataTransfer = dataTransfer;
        this.userGroupRepository = userGroupRepository;
    }

    public RegisterService(HttpServletRequest request, HttpSession session, UserDAO dataAccess, UserGroupRepository userGroupRepository) {
        this.request = request;
        this.session = session;
        this.dataAccess = dataAccess;
        this.userGroupRepository = userGroupRepository;
    }

    public final String init() {
        return "/html/register";
    }

    public final String register() {
		try {
            if (!dataTransfer.getUserId().isEmpty() && !dataTransfer.getUserPw().isEmpty()
                && !dataTransfer.getUserName().isEmpty() && !dataTransfer.getUserEmail().isEmpty()) {
                final String passwordSalt = Sha512.generateSalt();
                dataTransfer.setUserPwSalt(passwordSalt)
                        .setUserPw(Sha512.getEncrypt(dataTransfer.getUserPw(), passwordSalt));
                dataAccess.insert(dataTransfer);
                UserGroupEntity userGroup = UserGroupEntity.builder().userId(dataTransfer.getUserId())
                                                        .userGroup("USER")
                                                        .build();
                userGroupRepository.save(userGroup);
            } else {
                return "/error/register";
            }
		} catch (DataAccessException e) {
        }
        return "/login/action/register";
    }

    public final RegisterModel checkValue() {
        try {
            model = objectMapper.readValue(request.getHeader("registerCheck"), RegisterModel.class);
            switch(model.getType()) {
                case "user_id":
                    dataTransfer.setUserId(model.getContent());
                    break;
                case "user_pw":
                    dataTransfer.setUserPw(model.getContent());
                    break;
                case "user_name":
                    dataTransfer.setUserName(model.getContent());
                    break;
                case "user_email":
                    dataTransfer.setUserEmail(model.getContent());
                    break;
            }
            model.setCondition(false);
            if (dataAccess.selectByInput(dataTransfer).size() < 1) {
                model.setCondition(true);
            }
        } catch (JsonParseException e) {
        } catch (JsonMappingException e) {
        } catch (IOException e) {
        } catch (DataAccessException e) {
        } catch (SQLException e) {
        }

        return model;
    }

    public final String unregister() {
        try {
            if(session.getAttribute("Credential") != null) {
                dataTransfer.setUserId(session.getAttribute("Id").toString());
                List<UserDTO> result = dataAccess.selectByInput(dataTransfer);
                if(result.size() == 1) {
                    userGroupRepository.deleteById(result.get(0).getUserId());
                    dataAccess.delete(result.get(0));
                }
            }
        } catch (DataAccessException e) {
        } catch (SQLException e) {
        }
        
        return "/";
    }
}