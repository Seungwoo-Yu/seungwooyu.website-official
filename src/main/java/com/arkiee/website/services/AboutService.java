// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.services;

import org.springframework.stereotype.Service;

// Sets up for service.
@Service
public class AboutService {
    public AboutService() { }

    // Sets values up to use
    public final String init() {
        return "/html/about";
    }
}