// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.services;

import com.arkiee.website.models.entities.UserGroupEntity;
import com.arkiee.website.repositories.jpa.UserGroupRepository;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

// Sets up for service.
@Service
public class IndexService {
    private HttpServletResponse response;
    private HttpSession session;
    private UserGroupRepository repository;

    public IndexService() {
    }

    public IndexService(HttpServletResponse response, HttpSession session, UserGroupRepository repository) {
        this.response = response;
        this.session = session;
        this.repository = repository;
    }

    // Sets values up to use
    public final String init() {
        return "/html/index";
    }

    public final String getPermission() {
        if (session.getAttribute("Id") != null) {
            UserGroupEntity result = repository.findByUserId(session.getAttribute("Id").toString());
            if(result != null) {
                response.setHeader("Permission", result.getUserGroup());
            }
        } else {
            response.setHeader("Permission", "ANONYMOUS");
        }
        return "";
    }

    public final String getId() {
        if(session.getAttribute("Id") != null) {
            response.setHeader("Id", session.getAttribute("Id").toString());
        }
        return "";
    }
}