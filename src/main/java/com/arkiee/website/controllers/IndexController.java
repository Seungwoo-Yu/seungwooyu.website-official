// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.controllers;

import com.arkiee.website.repositories.jpa.UserGroupRepository;
import com.arkiee.website.services.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
    This controller is working as "Router" in Node.js
    which means it interacts with url by connecting their static sites.
    Meanwhile, it can provide data connection as well as they connect them.
*/
@Controller
public class IndexController {
    /*
        By using RequestMapping() on function, we can access and allocate the url
        which user wants to go in as "Router.get()" in Node.js.
        Therefore, It is divided by Model and Service;
        Model(Value Object) means the data which provides to View
        and Service means the way for manipulating Model.
        also, it is divided by DAO and DTO for interaction with Database;
        DAO(Data Access Object) means the temperated data to load data in Database
        and DTO(Data Transfer Object) the optimised data for transfer with DAO and even Service.
        DTO is known as VO as well as Model.

        Go to models/IndexModel.java to see what happens in Models.
        Go to services/IndexService.java to see what happens in Services.
        Go to dao/UserDAO.java to see what happens in DAO.
        Go to dto/UserDTO.java to see what happens in DTO.
    */
    @Autowired
    UserGroupRepository repository;
    /*
        Index Site
        @param None
        @return url
    */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexSite(HttpServletRequest request) {
        IndexService service = new IndexService();
        return service.init();
    }

    /*
        Forwarded url to bring session to javascript
        @param None
        @return url
    */
    @RequestMapping(value = "/session", method = RequestMethod.HEAD)
    @ResponseBody
    public String getSession() {
        return "";
    }

    @RequestMapping(value = "/permission", method = RequestMethod.HEAD)
    @ResponseBody
    public String getPermission(HttpServletResponse response, HttpSession session) {
        IndexService service = new IndexService(response, session, repository);
        return service.getPermission();
    }

    @RequestMapping(value = "/id", method = RequestMethod.HEAD)
    @ResponseBody
    public String getUserId(HttpServletResponse response, HttpSession session) {
        IndexService service = new IndexService(response, session, repository);
        return service.getId();
    }
}