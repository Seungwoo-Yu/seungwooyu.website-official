// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.controllers;

import com.arkiee.website.models.RegisterModel;
import com.arkiee.website.models.dto.UserDTO;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.repositories.jpa.UserGroupRepository;
import com.arkiee.website.services.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/register")
public class RegisterController {
    /*
        DAO and Repository are required to be called in controller with Autowired annotation.
    */
    @Autowired
    private UserDAO dataAccess;
    @Autowired
    private UserGroupRepository userGroupRepository;
    /*
        Registration Site
        @param UserDTO
        @return url
    */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String registerSite() {
        RegisterService service = new RegisterService(dataAccess);
        return service.init();
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    @ResponseBody
    public String unregister(HttpServletRequest request, HttpSession session) {
        RegisterService service = new RegisterService(request, session, dataAccess, userGroupRepository);
        return service.unregister();
    }

    /*
        Determine if input is existed in DB or not
        @param RegisterModel
        @return RegisterModel
    */
    @RequestMapping(value = "/check", method = RequestMethod.GET)
    @ResponseBody
    public RegisterModel registerCheck(HttpServletRequest request) {
        RegisterService service = new RegisterService(request, dataAccess);
        return service.checkValue();
    }

    /*
        Registration Process
        @param UserDTO
        @return url
    */
    @RequestMapping(value = "/process", method = RequestMethod.POST)
    @ResponseBody
    public String registerProcess(HttpServletRequest request, @RequestBody UserDTO userTransfer) {
        RegisterService service = new RegisterService(request, dataAccess, userTransfer, userGroupRepository);
        return service.register();
    }
}