// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorController_Implement implements ErrorController{
    @RequestMapping(value = "/error")
    public String errorSite() {
        return "/html/error";
    }

    @RequestMapping(value = "/error/*")
    public String errorSiteDesignated() {
        return "/html/error";
    }

    @Override
    public String getErrorPath() {
        return "redirect:/error";
    }
}