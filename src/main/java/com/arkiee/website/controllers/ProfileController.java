// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.controllers;

import com.arkiee.website.models.dto.UserDTO;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
    This controller is working as "Router" in Node.js
    which means it interacts with url by connecting their static sites.
    Meanwhile, it can provide data connection as well as they connect them.
*/
@Controller
@RequestMapping("/profile")
public class ProfileController {
    /*
        DAO is required to be called in controller with Autowired annotation.
    */
    @Autowired
    private UserDAO dataAccess;
    /*
        Profile Site
        @param None
        @return url
    */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String profileSite() {
        ProfileService service = new ProfileService();
        return service.init();
    }

    @RequestMapping("/failure")
    public String loginFailure() {
        return "forward:/profile";
    }

    @RequestMapping("/success")
    public String loginSuccess() {
        return "forward:/profile";
    }

    @RequestMapping(value = "/data", method = RequestMethod.GET)
    @ResponseBody
    public String getProfile(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        ProfileService service = new ProfileService(request, response, session, dataAccess);
        return service.getProfile();
    }

    @RequestMapping(value = "/password", method = RequestMethod.PATCH)
    @ResponseBody
    public String updatePassword(HttpServletRequest request, HttpServletResponse response, @RequestBody UserDTO dataTransfer) {
        ProfileService service = new ProfileService(request, response, dataTransfer, dataAccess);
        return service.updateProfile();
    }

    @RequestMapping(value = "/information", method = RequestMethod.PATCH)
    @ResponseBody
    public String updateInformation(HttpServletRequest request, HttpServletResponse response, @RequestBody UserDTO dataTransfer) {
        ProfileService service = new ProfileService(request, response, dataTransfer, dataAccess);
        return service.updateProfile();
    }
}