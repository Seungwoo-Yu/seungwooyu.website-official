// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.controllers;

import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
    This controller is working as "Router" in Node.js
    which means it interacts with url by connecting their static sites.
    Meanwhile, it can provide data connection as well as they connect them.
*/
@Controller
@RequestMapping("/login")
public class LoginController {
    /*
        DAO is required to be called in controller with Autowired annotation.
    */
    @Autowired
    private UserDAO dataAccess;
    /*
        Login Site
        @param IndexModel
        @return url
    */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String loginSite(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        LoginService service = new LoginService(request, response, session, dataAccess);
        return service.init();
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    @ResponseBody
    public String logout(HttpSession session, HttpServletRequest request) {
        LoginService service = new LoginService(session, request);
        return service.logout();
    }

    @RequestMapping("/action/*")
    public String loginAction() {
        return "forward:/login";
    }
    
    @RequestMapping("/failure")
    public String loginFailure() {
        return "forward:/login";
    }

    @RequestMapping(value = "/process", method = RequestMethod.GET)
    @ResponseBody
    public String loginProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        LoginService service = new LoginService(request, response, session, dataAccess);
        return service.login();
    }
}