// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.controllers;

import com.arkiee.website.models.CommentModel;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.repositories.jpa.CommentRepository;
import com.arkiee.website.repositories.jpa.UserGroupRepository;
import com.arkiee.website.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
    This controller is working as "Router" in Node.js
    which means it interacts with url by connecting their static sites.
    Meanwhile, it can provide data connection as well as they connect them.
*/
@Controller
@RequestMapping("/comments")
public class CommentController {
    @Autowired
    UserDAO userRepository;
    @Autowired
    CommentRepository repository;
    @Autowired
    UserGroupRepository userGroup;

    @RequestMapping(value = "/{number}", method = RequestMethod.GET)
    @ResponseBody
    public String getComments(HttpServletRequest request) {
        CommentService service = new CommentService(request, repository, userRepository);
        return service.getComments();
    }

    @RequestMapping(value = "/{number}", method = RequestMethod.POST)
    @ResponseBody
    public String addComment(HttpSession session, @RequestBody CommentModel commentModel) {
        CommentService service = new CommentService(session, repository, commentModel);
        return service.addComment();
    }

    @RequestMapping(value = "/{number}", method = RequestMethod.PATCH)
    @ResponseBody
    public String editComment(HttpSession session, @RequestBody CommentModel commentModel) {
        CommentService service = new CommentService(session, repository, commentModel, userGroup);
        return service.editComment();
    }

    @RequestMapping(value = "/{number}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteComment(HttpSession session, @RequestBody CommentModel commentModel) {
        CommentService service = new CommentService(session, repository, commentModel, userGroup);
        return service.deleteComment();
    }
}