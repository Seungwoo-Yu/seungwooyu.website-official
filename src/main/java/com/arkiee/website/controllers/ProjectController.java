// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.controllers;

import com.amazonaws.services.s3.AmazonS3;
import com.arkiee.website.models.FileModel;
import com.arkiee.website.models.ProjectModel;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.repositories.jpa.ProjectRepository;
import com.arkiee.website.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
    This controller is working as "Router" in Node.js
    which means it interacts with url by connecting their static sites.
    Meanwhile, it can provide data connection as well as they connect them.
*/
@Controller
@RequestMapping("/projects")
public class ProjectController {
    @Autowired
    private ProjectRepository repository;
    @Autowired
    private AmazonS3 amazon;
    @Autowired
    private UserDAO userAccess;
    /*
        Amazon and directory setting
    */
    @Autowired
    @Qualifier(value = "bucketName")
    private String bucketName;
    @Value("${server.upload.cache-directory}")
    private String cacheDirectory;
    @Value("${server.upload.project-directory-prefix}")
    private String projectDirectoryPrefix;
    /*
        Project Site
        @param None
        @return url
    */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String projectSite() {
        ProjectService service = new ProjectService();
        return service.init();
    }

    @RequestMapping(value = "/post", method = RequestMethod.GET)
    public String addPost() {
        ProjectService service = new ProjectService();
        return service.addPost();
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public String getProjectList(HttpServletRequest request) {
        ProjectService service = new ProjectService(request, repository, userAccess);
        return service.getProjectList();
    }

    @RequestMapping(value = "/{searchValue}", method = RequestMethod.GET, headers = {"content-type=application/x-form-urlencoded"})
    @ResponseBody
    public String searchProject(HttpServletRequest request) {
        ProjectService service = new ProjectService(request, repository, userAccess);
        return service.searchProject();
    }

    @RequestMapping(value = "/{number}", method = RequestMethod.GET)
    public String viewProject() {
        ProjectService service = new ProjectService();
        return service.viewPost();
    }

    @RequestMapping(value = "/{number}/edit", method = RequestMethod.GET)
    public String editPost() {
        ProjectService service = new ProjectService();
        return service.editPost();
    }

    @RequestMapping(value = "/{number}/contents", method = RequestMethod.GET)
    @ResponseBody
    public String getProjectContents(HttpServletRequest request) {
        ProjectService service = new ProjectService(request, repository, userAccess);
        return service.getProjectContents();
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    @ResponseBody
    public String postProject(HttpSession session, @RequestBody ProjectModel model) {
        ProjectService service = new ProjectService(session, repository, model);
        return service.postProject();
    }

    @RequestMapping(value = "/file", method = RequestMethod.POST)
    @ResponseBody
    public String fileUpload(@RequestParam("file") MultipartFile[] file, @RequestParam("directory") String directory) {
        FileModel fileModel = FileModel.builder().file(file)
                                                .directory(directory)
                                                .build();
        ProjectService service = new ProjectService(amazon, bucketName, cacheDirectory, projectDirectoryPrefix, fileModel);
        return service.fileUpload();
    }

    @RequestMapping(value = "/{number}", method = RequestMethod.PATCH)
    @ResponseBody
    public String updateProject(HttpSession session, @RequestBody ProjectModel model) {
        ProjectService service = new ProjectService(session, repository, model);
        return service.updateProject();
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteProjects(HttpSession session, @RequestBody String string) {
        ProjectService service = new ProjectService(amazon, bucketName, projectDirectoryPrefix, session, repository);
        return service.deleteProjects(string);
    }

    @RequestMapping(value = "/{number}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteProject(HttpSession session, @PathVariable("number") Integer number) {
        ProjectService service = new ProjectService(amazon, bucketName, projectDirectoryPrefix, session, repository);
        return service.deleteProject(number);
    }
}