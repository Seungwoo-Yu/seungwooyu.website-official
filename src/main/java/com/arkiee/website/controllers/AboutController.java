// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.controllers;

import com.arkiee.website.services.AboutService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/*
    This controller is working as "Router" in Node.js
    which means it interacts with url by connecting their static sites.
    Meanwhile, it can provide data connection as well as they connect them.
*/
@Controller
@RequestMapping("/about")
public class AboutController {
    /*
        About Site
        @param None
        @return url
    */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String aboutSite() {
        AboutService service = new AboutService();
        return service.init();
    }
    
}