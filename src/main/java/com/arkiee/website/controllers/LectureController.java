// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.controllers;

import com.amazonaws.services.s3.AmazonS3;
import com.arkiee.website.models.FileModel;
import com.arkiee.website.models.LectureModel;
import com.arkiee.website.repositories.dao.UserDAO;
import com.arkiee.website.repositories.jpa.LectureRepository;
import com.arkiee.website.services.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
    This controller is working as "Router" in Node.js
    which means it interacts with url by connecting their static sites.
    Meanwhile, it can provide data connection as well as they connect them.
*/
@Controller
@RequestMapping("/lectures")
public class LectureController {
    @Autowired
    private LectureRepository repository;
    @Autowired
    private AmazonS3 amazon;
    @Autowired
    private UserDAO userAccess;
    /*
        Amazon and directory setting
    */
    @Autowired
    @Qualifier(value = "bucketName")
    private String bucketName;
    @Value("${server.upload.cache-directory}")
    private String cacheDirectory;
    @Value("${server.upload.lecture-directory-prefix}")
    private String lectureDirectoryPrefix;
    /*
        Lecture Site
        @param None
        @return url
    */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String lectureSite() {
        LectureService service = new LectureService();
        return service.init();
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public String getLectureList(HttpServletRequest request, HttpServletResponse response) {
        LectureService service = new LectureService(request, repository, userAccess);
        return service.getLectureList();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public String postLecture(HttpSession session, @RequestBody LectureModel model) {
        LectureService service = new LectureService(session, repository, model);
        return service.postLecture();
    }

    @RequestMapping(value = "/file", method = RequestMethod.POST)
    @ResponseBody
    public String fileUpload(@RequestParam("file") MultipartFile[] file, @RequestParam("directory") String directory) {
        FileModel fileModel = FileModel.builder().file(file)
                                                .directory(directory)
                                                .build();
        LectureService service = new LectureService(amazon, bucketName, cacheDirectory, lectureDirectoryPrefix, fileModel);
        return service.fileUpload();
    }

    @RequestMapping(value = "/{searchValue}", method = RequestMethod.GET, headers = {"content-type=application/x-form-urlencoded"})
    @ResponseBody
    public String searchProject(HttpServletRequest request) {
        LectureService service = new LectureService(request, repository, userAccess);
        return service.searchLecture();
    }

    @RequestMapping(value = "/{number}", method = RequestMethod.PATCH)
    @ResponseBody
    public String updateLecture(HttpSession session, @RequestBody LectureModel model) {
        LectureService service = new LectureService(session, repository, model);
        return service.updateLecture();
    }

    @RequestMapping(value = "/{number}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteLecture(HttpSession session, @PathVariable("number") Integer number) {
        LectureService service = new LectureService(amazon, bucketName, lectureDirectoryPrefix, session, repository);
        return service.deleteLecture(number);
    }
}