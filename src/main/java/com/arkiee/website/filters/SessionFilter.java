// © 2019 Arkiee(Seungwoo Yu) - All Rights Reserved.
package com.arkiee.website.filters;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SessionFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        if(request.getSession().getAttribute("Credential") != null) {
            response.setHeader("Credential", request.getSession().getAttribute("Credential").toString());
        }
    }
}